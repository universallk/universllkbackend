#!/bin/sh

# Path to the application
APP=mvideolknew
APPLICATION_PATH=/opt/apps/$APP
APPLICATION=/opt/apps/$APP/$APP"*.jar"

case "$1" in
    start)
        echo "Starting the $APP..."
		rm -f ${APPLICATION_PATH}/pid
        java -jar ${APPLICATION} 1>/dev/null 2>/dev/null & echo $! >>pid
		echo "The $APP has started."
        ;;
    stop)
		echo "Stopping the $APP..."
		sudo kill -15 $(cat pid)
		echo "The $APP has stopped."
        ;;
    restart)
	echo "Stopping the $APP..."
	sudo kill -15 $(cat pid)
	echo "The $APP has stopped."
	echo "Starting the $APP..."
	rm -f ${APPLICATION_PATH}/pid
        java -jar ${APPLICATION} 1>/dev/null 2>/dev/null & echo $! >>pid
	echo "The $APP has started."
	;;
    *)
        echo "Usage: $SCRIPTNAME {start|stop|restart}" >&2
        exit 1
        ;;
esac

exit 0
