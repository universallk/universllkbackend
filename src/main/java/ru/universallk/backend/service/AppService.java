package ru.universallk.backend.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.dto.AppDTO;

import java.util.Optional;

/**
 * Service Interface for managing {@link App}.
 */
public interface AppService {

    /**
     * Save a app.
     *
     * @param appDTO the entity to save.
     * @return the persisted entity.
     */
    AppDTO save(AppDTO appDTO);

    /**
     * Get all the apps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AppDTO> findAll(Pageable pageable);


    /**
     * Get the "id" app.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AppDTO> findOne(Long id);

    /**
     * Delete the "id" app.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
