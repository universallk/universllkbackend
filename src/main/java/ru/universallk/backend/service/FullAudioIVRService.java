package ru.universallk.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.service.dto.AppJDBC;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.audio.FullAudioIvr;
import ru.universallk.backend.service.mapper.FullAudioIvrRowMapper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * todo: сервис реализован только для лк мвидео, т.к. реализация контуров была сделана через сраку,
 * каким-то дебилом, а потом ещё и выборка данных была сделана не через sql а через стримы,
 * за такое надо к стенке нахуй и расстреливать к ебеням собачьим
 */
@Service
public class FullAudioIVRService {
    private final CachingJdbcTemplate cachingJdbcTemplate;
    private final Logger log = LoggerFactory.getLogger(FullAudioIVRService.class);

    public FullAudioIVRService(CachingJdbcTemplate cachingJdbcTemplate) {
        this.cachingJdbcTemplate = cachingJdbcTemplate;
    }

    public Page<FullAudioIvr> findAll(FilterTotal filterTotal, Pageable pageable) {
        boolean[] isAnd = {false};
        if (this.cachingJdbcTemplate.getCaching().size() > 0) {

            StringBuilder queryParams = new StringBuilder();
            if (filterTotal.getId() != null) {
                AppJDBC appJDBC = this.cachingJdbcTemplate.getCaching().get(filterTotal.getId());
                this.appendDate(queryParams, filterTotal, isAnd);  //проставляем даты с по
                this.setAnd(queryParams, isAnd);                     // поставим and
                queryParams.append(appJDBC.getApp().getKonturs().stream()    // добавляем список контуров
                        .map(Kontur::getKonturNumber).map(Object::toString).
                                collect(Collectors.joining(",", " app_id IN (", ")")));
                isAnd[0] = true;
                this.appendPhone(queryParams, filterTotal, isAnd);               // проставляем телефон
                this.appendLengthPhone(queryParams, isAnd);                      //д линну телефона
                queryParams.append(" AND disconnect_reason != 'error' ");          //не равно еррор
                this.appendTheme(queryParams, filterTotal);                       // просталвяем тематику
                this.appendMessage(queryParams, filterTotal);                       // просталвяем озвучено или нет
                this.appendVdn(queryParams, filterTotal);
                queryParams.append(" ORDER BY connect_date desc ");
                Long count = null;
                if (filterTotal.getCountAll() == null || filterTotal.getCountAll() == 0L) {
                    count = this.returnCount(queryParams.toString(), appJDBC.getJdbcTemplate());
                } else {
                    count = filterTotal.getCountAll();
                }
                queryParams.append(" LIMIT ").append(pageable.getPageNumber() * pageable.getPageSize()).append(", ").
                        append(pageable.getPageSize());
               String sql = "SELECT * ,    TIMEDIFF(disconnect_date, connect_date) duration_time,\n" +
                       "       mvideo_reports.get_last_theme(id)   as last_theme," +
                       "       mvideo_reports.get_last_extradata(id)    as extradata,\n" +
                       "       mvideo_reports.get_dialog_sounded_message(id) \tas  sounded_message\n" +
                       " FROM app_statistic.call where " +  queryParams.toString();
                log.info("запрос фильтра {}", sql);
                return this.queryFiler(sql, appJDBC, pageable, count);
            } else {
                log.error("необходимо выбрать приложение");
                return null;
            }
        } else {
            log.error("все приложения выключены");
            return null;
        }
    }

    private void appendVdn(StringBuilder queryParams, FilterTotal filterTotal) {
        if (filterTotal.getSkill() !=null) {
            queryParams.append(" and mvideo_reports.get_last_vdn(id) like '")
                    .append(filterTotal.getSkill()).append("' ");
        }
    }

    private void appendMessage(StringBuilder queryParams, FilterTotal filterTotal) {
        if (filterTotal.getSoundMessage()!=null) {
            if (filterTotal.getSoundMessage().equals("sounded_message")) {
                queryParams.append(" and mvideo_reports.get_dialog_sounded_message(id) not like ' ' ");
            } else {
                queryParams.append(" and mvideo_reports.get_dialog_sounded_message(id) like ' ' ");
            }
        }
    }

    private void appendTheme(StringBuilder queryParams, FilterTotal filterTotal) {
        if (filterTotal.getTheme() != null) {
            queryParams.append("and mvideo_reports.get_last_theme(id) like '"
            ).append(filterTotal.getTheme()).append("' ");
        }
    }

    /**
     * выполнение запроса по подготовленным параметрам
     */
    private Page<FullAudioIvr> queryFiler(String sql, AppJDBC appJDBC, Pageable pageable, Long count) {
        List<FullAudioIvr> fullAudios = new ArrayList<>();
        try {
            fullAudios = new ArrayList<>(appJDBC.getJdbcTemplate().query(sql,
                    new FullAudioIvrRowMapper(appJDBC.getApp().getName(), appJDBC.getApp().getSkills(),
                            appJDBC.getApp().getThemes())));
        } catch (DataAccessException e) {
            log.error(e.getMessage(), e);
        }
        return new PageImpl<>(fullAudios, pageable, count);
    }

    private void appendDate(StringBuilder sql, FilterTotal filterTotal, boolean[] isAnd) {
        if (filterTotal.getDateStart() != null) {
            log.info(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateStart())));
            sql.append("  connect_date >= '").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateStart()))).append("' ");
            isAnd[0] = true;
        }
        if (filterTotal.getDateEnd() != null) {
            this.setAnd(sql, isAnd);
            sql.append(" connect_date < '").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateEnd()))).append("' ");
            isAnd[0] = true;
        }
    }

    private void appendPhone(StringBuilder sql, FilterTotal filterTotal, boolean[] isAnd) {
        if (filterTotal.getPhone() != null && !filterTotal.getPhone().equals("")) {
            this.setAnd(sql, isAnd);
            sql.append("phone like '%").append(filterTotal.getPhone()).append("%' ");
            isAnd[0] = true;
        }
    }

    private void appendLengthPhone(StringBuilder sql, boolean[] isAnd) {
        log.debug("isAnd = " + isAnd[0]);
        this.setAnd(sql, isAnd);
        sql.append(" char_length(phone) >= 10 ");
    }

    private void setAnd(StringBuilder sql, boolean[] isAnd) {
        if (isAnd[0]) {
            sql.append(" AND ");
        }
    }

    /**
     * общее количество
     */
    private Long returnCount(String sqlRsl, JdbcTemplate jdbcTemplate) {
        String newSql = "SELECT count(0)  FROM app_statistic.call where "  + sqlRsl;
        log.info("запрос для общего количества = {} ", newSql);
        return jdbcTemplate.queryForList(newSql,
                Long.class
        ).stream().findFirst().orElse(0L);
    }

}
