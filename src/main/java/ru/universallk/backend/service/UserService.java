package ru.universallk.backend.service;

import ru.universallk.backend.domain.User;
import ru.universallk.backend.repository.UserRepository;
import ru.universallk.backend.service.dto.UserDTO;
import ru.universallk.backend.service.dto.UserFilter;
import ru.universallk.backend.service.mapper.UserMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PrepareUserService prepareUserService;

    UserService(UserRepository userRepository,
                UserMapper userMapper,
                PrepareUserService prepareUserService) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.prepareUserService = prepareUserService;
    }

    /**
     * получить всех пользаков c пагинацией  (гадо пдумать как реализовать)
     *
     * @param pageable пагинация
     * @return {@link UserDTO}
     */
    public Page<UserDTO> findAll(UserFilter criteria, Pageable pageable) {
        return this.userRepository.findAll(criteria.buildCriteria(), pageable).map(this.userMapper::userToUserDTO);
    }

    /**
     * создание пользователя через админку
     *
     * @param userDTO {@link UserDTO}
     * @return {@link UserDTO}
     */
    public UserDTO create(UserDTO userDTO) {
        return this.prepareUserService.prepareUserSave(userDTO, () -> {
                    User us = this.prepareUserService.encode(userDTO);
                    User res = this.userRepository.save(us);
                    return this.userMapper.userToUserDTO(res);
                }
        );
    }
    /**
     * обновление данных текущего пользователя
     *
     * @param userDTO {@link UserDTO}
     * @return {@link UserDTO}
     */
    public UserDTO updateUser(UserDTO userDTO) {
        return this.userMapper.userToUserDTO(this.userRepository.save(
                this.prepareUserService.prepareUserUpdate(userDTO)));
    }
    /**
     * получить пользователя по логину
     *
     * @param login логин пользователя
     * @return {@link UserDTO}
     */
    public UserDTO getUserByLogin(String login) {
        return this.userMapper.userToUserDTO(this.userRepository.findByLogin(login).orElse(new User()));
    }



}
