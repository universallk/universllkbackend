package ru.universallk.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.domain.DeleteResource;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Alexander Kaleanov
 * класс для удаления временных файлов, когда они сконвертированны необходимо хранить временный файлы не более 2 минут
 * чтобы пользователю успевали доходить все файлы
 */
@Service
public class DeleteFileService {
    private final DeleteResourceService deleteResourceService;
    private final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(10);
    private final Logger log = LoggerFactory.getLogger(DeleteFileService.class);

    public DeleteFileService(DeleteResourceService deleteResourceService) {
        this.deleteResourceService = deleteResourceService;
    }

    /**
     * добавление на удаление
     * @param resource ресурс
     * @param deleteResource кидаем в бд
     */
    private void pushToList(Resource resource, DeleteResource deleteResource) {
        if (resource != null) {
            scheduler.schedule(() -> {
                        if (resource.exists() || resource.isReadable()) {
                            try {
                                if (resource.getFile().delete()) {
                                    log.info("временный файл {} удалён", resource.getFilename());
                                }
                            } catch (IOException e) {
                                log.error(e.getMessage(), e);
                            }
                        }
                        this.deleteResourceService.deleteResource(deleteResource);
                    }, 120, TimeUnit.SECONDS
            );

        }
    }

    @Transactional
    public void pushToList(Resource resource) {
        if (resource != null) {
            DeleteResource deleteResource;
            try {
                deleteResource = new DeleteResource(resource.getFile().getAbsolutePath());
                this.pushToList(resource, this.deleteResourceService.save(deleteResource));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Transactional
    public void deleteResource() {
        this.deleteResourceService.findAl().forEach(e -> {
            try {
                this.pushToList(new UrlResource(Paths.get(e.getPath()).toUri()), e);
            } catch (MalformedURLException malformedURLException) {
                this.deleteResourceService.deleteResource(e);
                log.error(malformedURLException.getMessage(), e);
            }
        });
    }
}
