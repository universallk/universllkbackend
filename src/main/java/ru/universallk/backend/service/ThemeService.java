package ru.universallk.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.domain.App_;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.domain.Theme_;
import ru.universallk.backend.repository.ThemeRepository;
import ru.universallk.backend.service.dto.ThemeCriteria;
import ru.universallk.backend.service.dto.ThemeDTO;
import ru.universallk.backend.service.filterUtil.QueryService;
import ru.universallk.backend.service.mapper.ThemeMapper;

import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ThemeService extends QueryService<Theme> {
    private final ThemeRepository themeRepository;
    private final ThemeMapper themeMapper;
    private final CachingJdbcTemplate cachingJdbcTemplate;

    public ThemeService(ThemeRepository themeRepository, ThemeMapper themeMapper, CachingJdbcTemplate cachingJdbcTemplate) {
        this.themeRepository = themeRepository;
        this.themeMapper = themeMapper;
        this.cachingJdbcTemplate = cachingJdbcTemplate;
    }

    public Optional<ThemeDTO> findOne(Long id) {
        return this.themeRepository.findById(id).map(this.themeMapper::toDto);
    }

    /**
     * сохранить
     *
     * @param themeDTO {@link ThemeDTO}
     * @return {@link ThemeDTO}
     */
    @Transactional
    public ThemeDTO save(ThemeDTO themeDTO) {
        ThemeDTO themeDTO1 =  this.themeMapper.toDto(this.themeRepository.save(this.themeMapper.toEntity(themeDTO)));
        this.cachingJdbcTemplate.modifyTheme(themeDTO1.getAppId());
        return themeDTO1;
    }

    public Page<ThemeDTO> findByCriteria(ThemeCriteria criteria, Pageable pageable) {
        Specification<Theme> specification = this.createSpecification(criteria);
        return this.themeRepository.findAll(specification, pageable).map(this.themeMapper::toDto);
    }

    public List<ThemeDTO> findByCriteria(ThemeCriteria criteria) {
        Specification<Theme> specification = this.createSpecification(criteria);
        return this.themeRepository.findAll(specification).stream().map(this.themeMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public void delete(Long id) {
        Long appId = this.themeRepository.findById(id).orElse(new Theme()).getApp().getId();
        this.themeRepository.deleteById(id);
        if (appId!=null) {
            this.cachingJdbcTemplate.modifyTheme(appId);
        }
    }

    private Specification<Theme> createSpecification(ThemeCriteria themeCriteria) {
        Specification<Theme> specification = Specification.where(null);
        if (specification != null) {
            if (themeCriteria.getName() != null) {
                specification = specification.and(buildStringSpecification(themeCriteria.getName(), Theme_.name));
            }
            if (themeCriteria.getName() != null) {
                specification = specification.and(
                        buildSpecification(themeCriteria.getAppId(),
                                root -> root.join(Theme_.app, JoinType.LEFT).get(App_.id))
                );
            }
        }
        return specification;
    }

}
