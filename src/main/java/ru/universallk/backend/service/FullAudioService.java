package ru.universallk.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.service.dto.AppJDBC;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.audio.FullAudio;
import ru.universallk.backend.service.mapper.FullAudioRowMapper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FullAudioService {
    private final CachingJdbcTemplate cachingJdbcTemplate;
    private final Logger log = LoggerFactory.getLogger(FullAudioService.class);

    public FullAudioService(CachingJdbcTemplate cachingJdbcTemplate) {
        this.cachingJdbcTemplate = cachingJdbcTemplate;
    }

    public Page<FullAudio> findAll(FilterTotal filterTotal, Pageable pageable) {
        boolean[] isAnd = {false};
        if (this.cachingJdbcTemplate.getCaching().size() > 0) {

            StringBuilder queryParams = new StringBuilder("SELECT c.*  FROM app_statistic.call as c WHERE ");
            if (filterTotal.getId() != null) {
                AppJDBC appJDBC = this.cachingJdbcTemplate.getCaching().get(filterTotal.getId());
                this.appendDate(queryParams, filterTotal, isAnd);
                this.appendPhone(queryParams, filterTotal, isAnd);
                this.appendLengthPhone(queryParams, isAnd);
                queryParams.append(appJDBC.getApp().getKonturs().stream()
                        .map(Kontur::getKonturNumber).map(Object::toString).
                                collect(Collectors.joining(",", " AND app_id IN (", ")")));
                queryParams.append(" AND c.disconnect_reason != 'error' " +
                        " ORDER BY c.connect_date desc ");
                Long count = null;
                if (filterTotal.getCountAll() == null || filterTotal.getCountAll() == 0L) {
                    count = this.returnCount(queryParams.toString(), appJDBC.getJdbcTemplate());
                } else {
                    count = filterTotal.getCountAll();
                }
                queryParams.append(" LIMIT ").append(pageable.getPageNumber() * pageable.getPageSize()).append(", ").
                        append(pageable.getPageSize());
                log.info("запрос фильтра {}", queryParams.toString());
                return this.queryFiler(queryParams.toString(), appJDBC, pageable, count);
            } else {
                log.error("необходимо выбрать приложение");
                return null;
            }
        } else {
            log.error("все приложения выключены");
            return null;
        }
    }

    /**
     * выполнение запроса по подготовленным параметрам
     */
    private Page<FullAudio> queryFiler(String sql, AppJDBC appJDBC, Pageable pageable, Long count) {
        List<FullAudio> fullAudios = new ArrayList<>();
        try {
            fullAudios = new ArrayList<>(appJDBC.getJdbcTemplate().query(sql, new FullAudioRowMapper(appJDBC.getApp().getName())));
        } catch (DataAccessException e) {
            log.error(e.getMessage(), e);
        }
        return new PageImpl<>(fullAudios, pageable, count);
    }

    private void appendDate(StringBuilder sql, FilterTotal filterTotal, boolean[] isAnd) {
        if (filterTotal.getDateStart() != null) {
            log.info(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateStart())));
            sql.append("  connect_date >= '").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateStart()))).append("' ");
            isAnd[0] = true;
        }
        if (filterTotal.getDateEnd() != null) {
            this.setAnd(sql, isAnd);
            sql.append(" connect_date < '").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateEnd()))).append("' ");
            isAnd[0] = true;
        }
    }

    private void appendPhone(StringBuilder sql, FilterTotal filterTotal, boolean[] isAnd) {
        if (filterTotal.getPhone() != null && !filterTotal.getPhone().equals("")) {
            this.setAnd(sql, isAnd);
            sql.append("phone like '%").append(filterTotal.getPhone()).append("%' ");
            isAnd[0] = true;
        }
    }

    private void appendLengthPhone(StringBuilder sql, boolean[] isAnd) {
        log.debug("isAnd = "  + isAnd[0]);
        this.setAnd(sql, isAnd);
        sql.append(" char_length(phone) >= 10 ");
    }

    private void setAnd(StringBuilder sql, boolean[] isAnd) {
        if (isAnd[0]) {
            sql.append(" AND ");
        }
    }

    /**
     * общее количество
     */
    private Long returnCount(String sqlRsl, JdbcTemplate jdbcTemplate) {
        String newSql = sqlRsl.replace(" c.* ", " count(0) ");
        log.info(newSql);
        return jdbcTemplate.queryForList(newSql,
                Long.class
        ).stream().findFirst().orElse(0L);
    }

}
