package ru.universallk.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.domain.App_;
import ru.universallk.backend.domain.Skill;
import ru.universallk.backend.domain.Skill_;
import ru.universallk.backend.repository.SkillRepository;
import ru.universallk.backend.service.dto.SkillCriteria;
import ru.universallk.backend.service.dto.SkillDTO;
import ru.universallk.backend.service.filterUtil.QueryService;
import ru.universallk.backend.service.mapper.SkillMapper;

import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SkillService extends QueryService<Skill> {
    private final SkillMapper skillMapper;
    private final SkillRepository skillRepository;
    private final CachingJdbcTemplate cachingJdbcTemplate;

    public SkillService(SkillMapper skillMapper, SkillRepository skillRepository, CachingJdbcTemplate cachingJdbcTemplate) {
        this.skillMapper = skillMapper;
        this.skillRepository = skillRepository;
        this.cachingJdbcTemplate = cachingJdbcTemplate;
    }

    @Transactional
    public SkillDTO save(SkillDTO skillDTO) {
        SkillDTO skillDTO1 = this.skillMapper.toDto(this.skillRepository.save(this.skillMapper.toEntity(skillDTO)));
        this.cachingJdbcTemplate.modifySkill(skillDTO1.getAppId());
        return skillDTO1;
    }

    public Optional<SkillDTO> findOne(Long id) {
        return this.skillRepository.findById(id).map(this.skillMapper::toDto);
    }

    public Page<SkillDTO> findByCriteria(SkillCriteria criteria, Pageable pageable) {
        Specification<Skill> specification = this.createSpecification(criteria);
        return this.skillRepository.findAll(specification, pageable).map(this.skillMapper::toDto);
    }

    public List<SkillDTO> findByCriteria(SkillCriteria criteria) {
        Specification<Skill> specification = this.createSpecification(criteria);
        return this.skillRepository.findAll(specification).stream().map(this.skillMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public void delete(Long id) {
        Long appId = this.skillRepository.findById(id
        ).orElse(new Skill()).getApp().getId();
        this.skillRepository.deleteById(id);
        if (appId!=null) {
            this.cachingJdbcTemplate.modifySkill(appId);
        }
    }

    private Specification<Skill> createSpecification(SkillCriteria skillCriteria) {
        Specification<Skill> specification = Specification.where(null);
        if (specification != null) {
            if (skillCriteria.getName() != null) {
                specification = specification.and(buildStringSpecification(skillCriteria.getName(), Skill_.name));
            }
            if (skillCriteria.getId() != null) {
                specification = specification.and(
                        buildSpecification(skillCriteria.getAppId(),
                                root -> root.join(Skill_.app, JoinType.LEFT).get(App_.id))
                );
            }
        }
        return specification;
    }

}
