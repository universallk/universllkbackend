package ru.universallk.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.App_;
import ru.universallk.backend.repository.AppRepository;
import ru.universallk.backend.service.dto.AppCriteria;
import ru.universallk.backend.service.dto.AppDTO;
import ru.universallk.backend.service.filterUtil.QueryService;
import ru.universallk.backend.service.mapper.AppMapper;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class AppQueryService extends QueryService<App> {

    private final Logger log = LoggerFactory.getLogger(AppQueryService.class);

    private final AppRepository appRepository;

    private final AppMapper appMapper;

    public AppQueryService(AppRepository appRepository, AppMapper appMapper) {
        this.appRepository = appRepository;
        this.appMapper = appMapper;
    }

    @Transactional(readOnly = true)
    public List<AppDTO> findByCriteria(AppCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<App> specification = createSpecification(criteria);
        return appMapper.toDto(appRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AppDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AppDTO> findByCriteria(AppCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<App> specification = createSpecification(criteria);
        return appRepository.findAll(specification, page)
            .map(appMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AppCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<App> specification = createSpecification(criteria);
        return appRepository.count(specification);
    }

    /**
     * Function to convert {@link AppCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<App> createSpecification(AppCriteria criteria) {
        Specification<App> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), App_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), App_.name));
            }
            if (criteria.getDataBase() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataBase(), App_.dataBase));
            }
            if (criteria.getAppType() != null) {
                specification = specification.and(buildSpecification(criteria.getAppType(), App_.appType));
            }

            if (criteria.getIsEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getIsEnabled(), App_.isEnabled));
            }
          
        }
        return specification;
    }
}
