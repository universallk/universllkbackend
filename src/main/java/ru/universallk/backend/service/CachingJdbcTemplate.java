package ru.universallk.backend.service;

import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.repository.SkillRepository;
import ru.universallk.backend.repository.ThemeRepository;
import ru.universallk.backend.service.dto.AppJDBC;

import java.util.concurrent.ConcurrentHashMap;

@Service
public class CachingJdbcTemplate {
    private final ThemeRepository themeRepository;
    private final SkillRepository skillRepository;
    /**
     * здесь содержаться jdbc темплейты по id  в списке моей бд
     */
    private final ConcurrentHashMap<Long, AppJDBC> caching = new ConcurrentHashMap<>();
    private final JdbcTemplateCreator jdbcTemplateCreator;

    public CachingJdbcTemplate(ThemeRepository themeRepository, SkillRepository skillRepository, JdbcTemplateCreator jdbcTemplateCreator) {
        this.themeRepository = themeRepository;
        this.skillRepository = skillRepository;
        this.jdbcTemplateCreator = jdbcTemplateCreator;
    }

    public ConcurrentHashMap<Long, AppJDBC> getCaching() {
        return caching;
    }

    /**
     * удалить из кеша
     * @param id {@link App#getId()}
     */
    public void removeFromCache(Long id) {
        caching.remove(id);
    }

    public void modify(App app) {
        if (app.getIsEnabled()) {
            this.caching.put(app.getId(), new AppJDBC(app, this.jdbcTemplateCreator.createJdbcTemplate(app)));
        } else {
            this.removeFromCache(app.getId());
        }
    }

    public void modifySkill(Long appId) {
        if (this.caching.get(appId)!=null) {
            this.caching.get(appId).getApp().setSkills(this.skillRepository.findAllByAppId(appId));
        }
    }

    public void modifyTheme(Long appId) {
             if (this.caching.get(appId)!=null) {
                 this.caching.get(appId).getApp().setThemes(this.themeRepository.findAllByAppId(appId));
     }
    }
}
