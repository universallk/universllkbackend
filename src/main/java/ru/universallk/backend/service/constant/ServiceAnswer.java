package ru.universallk.backend.service.constant;

import java.util.Arrays;
import java.util.List;

public class ServiceAnswer {
    /**
     * Время ответа сервисов поля
     */
    public final static String LOYALITY_API = "LOYALITY_API";
    public final static String STATUSES_API_HISTORY = "STATUSES_API_HISTORY";
    public final static String STATUSES_API_INFO = "STATUSES_API_INFO";
    public final static String RESERVE_ORDER_REST_REQUEST = "RESERVE_ORDER_REST_REQUEST";
    public final static String CANCEL_ORDER_REST_REQUEST = "CANCEL_ORDER_REST_REQUEST";
    public final static String STATUSES_CHANCEL_ORDER_REST_REQUEST = "STATUSES_CHANCEL_ORDER_REST_REQUEST";
    public final static String LOYALITY_API_VIEW = "Ответ сервиса SP_004_V3:(LOYALITY), (сек)";
    public final static String STATUSES_API_HISTORY_VIEW  = "Ответ сервиса SP_071_V1:(ORDER HISTORY), (сек)";
    public final static String STATUSES_API_INFO_VIEW  = "Ответ сервиса SP_070_V1:(ORDER DETAIL), (сек)";
    public final static String RESERVE_ORDER_REST_REQUEST_VIEW  = "Ответ сервиса SP_160_V1:(RESERVE), (сек)";
    public final static String CANCEL_ORDER_REST_REQUEST_VIEW  = "Ответ сервиса SP_158_V1:(CANCEL), (сек)";
    public final static String STATUSES_CHANCEL_ORDER_REST_REQUEST_VIEW  = "Ответ сервиса SP_159_V1:(REASONS), (сек)";

}
