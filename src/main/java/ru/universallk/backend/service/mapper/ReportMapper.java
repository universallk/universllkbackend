package ru.universallk.backend.service.mapper;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.service.dto.reports.OnlineIVR;
import ru.universallk.backend.service.constant.ServiceAnswer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * получаем список объектов по запросу
 */
public class ReportMapper implements RowMapper<OnlineIVR> {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final static String COLUMN_CONNECT_DATE = "connect_date";
    private final static String LAST_THEME = "last_theme";
    private final static String EXTRA_DATA = "extradata";

    private final List<Theme> themeList;

    public ReportMapper(List<Theme> themeList) {
        this.themeList = themeList;
    }

    @Override
    public OnlineIVR mapRow(ResultSet resultSet, int i) throws SQLException {
        OnlineIVR rowMAp = new OnlineIVR();
        rowMAp.setTheme(getThemeName(resultSet.getString(LAST_THEME)));
        Timestamp connectDate = resultSet.getTimestamp(COLUMN_CONNECT_DATE);
        if (connectDate != null) {
            rowMAp.setHour(new SimpleDateFormat("HH").format(connectDate));
        }
        try {
            this.timeAnswerByLoyalityApi(rowMAp, resultSet.getString(EXTRA_DATA));
        } catch (JSONException e) {
            logger.error(e.getMessage(), e);
        }
        return rowMAp;
    }
    private void timeAnswerByLoyalityApi(OnlineIVR rowMAp, String jsonExtraDat) throws JSONException {
        if (jsonExtraDat != null && !jsonExtraDat.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonExtraDat);
            if (jsonObj.has(ServiceAnswer.LOYALITY_API)) {
                rowMAp.setLOYALITY_API(
                        Double.valueOf(jsonObj.getString(ServiceAnswer.LOYALITY_API)));
            }
            if (jsonObj.has(ServiceAnswer.STATUSES_API_HISTORY)) {
                rowMAp.setSTATUSES_API_HISTORY(
                        Double.valueOf(jsonObj.getString(ServiceAnswer.STATUSES_API_HISTORY)));
            }
            if (jsonObj.has(ServiceAnswer.STATUSES_API_INFO)) {
                rowMAp.setSTATUSES_API_INFO(
                        Double.valueOf(jsonObj.getString(ServiceAnswer.STATUSES_API_INFO)));
            }
            if (jsonObj.has(ServiceAnswer.RESERVE_ORDER_REST_REQUEST)) {
                rowMAp.setRESERVE_ORDER_REST_REQUEST(
                        Double.valueOf(jsonObj.getString(ServiceAnswer.RESERVE_ORDER_REST_REQUEST)));
            }
            if (jsonObj.has(ServiceAnswer.CANCEL_ORDER_REST_REQUEST)) {
                rowMAp.setCANCEL_ORDER_REST_REQUEST(
                        Double.valueOf(jsonObj.getString(ServiceAnswer.CANCEL_ORDER_REST_REQUEST)));
            }
            if (jsonObj.has(ServiceAnswer.STATUSES_CHANCEL_ORDER_REST_REQUEST)) {
                rowMAp.setSTATUSES_CHANCEL_ORDER_REST_REQUEST(
                        Double.valueOf(jsonObj.getString(ServiceAnswer.STATUSES_CHANCEL_ORDER_REST_REQUEST)));
            }
        }
    }

    private String getThemeName(String value) {
        return this.themeList.stream().filter(e -> e.getValue().toLowerCase().equals(value.toLowerCase()))
                .findFirst().map(Theme::getName).orElse("Другое");
    }
}
