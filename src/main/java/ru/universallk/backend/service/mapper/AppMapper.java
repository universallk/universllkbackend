package ru.universallk.backend.service.mapper;


import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.dto.AppDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link App} and its DTO {@link AppDTO}.
 */
@Service
public class AppMapper implements EntityMapper<AppDTO, App> {
private final ConturMapper conturMapper;

    public AppMapper(ConturMapper conturMapper) {
        this.conturMapper = conturMapper;
    }

    public AppDTO toDto(App app) {
        if (app == null) {
            return null;
        } else {
            AppDTO appDTO = new AppDTO();
            appDTO.setName(app.getName());
            appDTO.setId(app.getId());
            appDTO.setIp(app.getIp());
            appDTO.setDataBase(app.getDataBase());
            appDTO.setKonturs(this.conturMapper.toDto(app.getKonturs()));
            appDTO.setPassword(app.getPassword());
            appDTO.setLogin(app.getLogin());
            appDTO.setEnabled(app.getIsEnabled());
            appDTO.setAppType(app.getAppType());
            return appDTO;
        }
    }

    @Override
    public List<App> toEntity(List<AppDTO> dtoList) {
        return dtoList == null ? new ArrayList<>() : dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<AppDTO> toDto(List<App> entityList) {
        return entityList == null ? new ArrayList<>() : entityList.stream().map(this::toDto).collect(Collectors.toList());
    }


    public App toEntity(AppDTO appDTO) {
        if (appDTO == null) {
            return null;
        } else {
            App app = new App();
            app.setId(appDTO.getId());
            app.setName(appDTO.getName());
            app.setIp(appDTO.getIp());
            app.setDataBase(appDTO.getDataBase());
            app.setKonturs(this.conturMapper.toEntity(appDTO.getKonturs()));
            app.setPassword(appDTO.getPassword());
            app.setLogin(appDTO.getLogin());
            app.setIsEnabled(appDTO.isEnabled());
            app.setAppType(appDTO.getAppType());
            return app;
        }
    }


    public App fromId(Long id) {
        if (id == null) {
            return null;
        }
        App app = new App();
        app.setId(id);
        return app;
    }
}
