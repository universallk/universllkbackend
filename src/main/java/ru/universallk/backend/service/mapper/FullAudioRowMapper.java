package ru.universallk.backend.service.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.universallk.backend.service.dto.audio.FullAudio;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Class DataRowMapper
 *
 * @author Kseniya Dergunova
 * @since 17.04.2020
 */
public class FullAudioRowMapper implements RowMapper<FullAudio> {

    private final static String COLUMN_ID = "id";
    private final static String COLUMN_APP_NAME = "app_name";
    private final static String COLUMN_APP_ID = "app_id";
    private final static String COLUMN_CONNECT_DATE = "connect_date";
    private final static String COLUMN_PHONE = "phone";
    private final static String COLUMN_RECORD_FILE = "record_file";
    private final String appName;

  public FullAudioRowMapper(String appName) {
        this.appName = appName;
    }

    @Override
    public FullAudio mapRow(ResultSet resultSet, int i) throws SQLException {
        FullAudio dataRow = new FullAudio();
        dataRow.setId(resultSet.getLong(COLUMN_ID));
        dataRow.setAppName(this.appName);
        dataRow.setAppId(resultSet.getLong(COLUMN_APP_ID));
        Timestamp connectDate = resultSet.getTimestamp(COLUMN_CONNECT_DATE);
        if (connectDate != null) {
            dataRow.setConnectDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(connectDate));
        }
        dataRow.setPhone(resultSet.getString(COLUMN_PHONE));
        dataRow.setRecordFile(resultSet.getString(COLUMN_RECORD_FILE));
        return dataRow;
    }
}
