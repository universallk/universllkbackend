package ru.universallk.backend.service.mapper;


import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.Role;
import ru.universallk.backend.domain.User;
import ru.universallk.backend.service.dto.AuthTokenResponseDTO;

import java.util.stream.Collectors;

@Service
public class AuthTokenResponseMapper {

    public AuthTokenResponseDTO toDTO(String jwtToken, User users) {
        AuthTokenResponseDTO authTokenResponseDTO = new AuthTokenResponseDTO();
        authTokenResponseDTO.setJwtToken(jwtToken);
        authTokenResponseDTO.setRoles(users.getRoles().stream().map(Role::getName).collect(Collectors.toList()));
        authTokenResponseDTO.setUsername(users.getLogin());
        return authTokenResponseDTO;
    }
}
