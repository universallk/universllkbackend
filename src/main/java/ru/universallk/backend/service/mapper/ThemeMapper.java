package ru.universallk.backend.service.mapper;


import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.service.dto.KonturDTO;
import ru.universallk.backend.service.dto.ThemeDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link App} and its DTO {@link ru.universallk.backend.service.dto.ThemeDTO}.
 */
@Service
public class ThemeMapper implements EntityMapper<ThemeDTO, Theme> {

    public ThemeDTO toDto(Theme theme) {
        if (theme == null) {
            return null;
        } else {
            ThemeDTO themeDTO = new ThemeDTO();
            themeDTO.setName(theme.getName());
            themeDTO.setId(theme.getId());
            themeDTO.setAppId(theme.getApp() != null ? theme.getApp().getId() : null);
            themeDTO.setValue(theme.getValue());
            return themeDTO;
        }
    }

    @Override
    public List<Theme> toEntity(List<ThemeDTO> dtoList) {
        return dtoList == null ? new ArrayList<>() : dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ThemeDTO> toDto(List<Theme> entityList) {
        return entityList == null ? new ArrayList<>() : entityList.stream().map(this::toDto).collect(Collectors.toList());
    }


    public Theme toEntity(ThemeDTO themeDTO) {
        if (themeDTO == null) {
            return null;
        } else {
            Theme theme = new Theme();
            theme.setName(themeDTO.getName());
            theme.setId(themeDTO.getId());
            theme.setApp(new App(themeDTO.getAppId()));
            theme.setValue(themeDTO.getValue());
            return theme;
        }
    }


    public Theme fromId(Long id) {
        if (id == null) {
            return null;
        }
        Theme theme = new Theme();
        theme.setId(id);
        return theme;
    }
}
