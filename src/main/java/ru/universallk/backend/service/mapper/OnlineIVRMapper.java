package ru.universallk.backend.service.mapper;

import lombok.var;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.service.constant.ServiceAnswer;
import ru.universallk.backend.service.dto.reports.OnlineIVR;
import ru.universallk.backend.service.dto.reports.OnlineIVRDTO;
import ru.universallk.backend.service.dto.reports.ThreeFunction;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * сервис для подготовки отчёта из хешкарт и List<OnlineIVR> entity
 */
@Service
public class OnlineIVRMapper {
    public final static List<String> hours = Arrays.asList("00", "01", "02", "03", "04", "05", "06", "07", "08",
            "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23");

    /**
     * метод ля компоновки данных
     *
     * @param entity данные полученные на основании sql запроса
     * @param themes список возможных тематик
     * @return готовые данные для отчёта
     */
    public List<OnlineIVRDTO> toDto(List<OnlineIVR> entity, List<Theme> themes) {
        List<OnlineIVRDTO> res = new ArrayList<>();
        // вся статистика с  разбиением по тематикам и часам
        // Map<Тематика, Map<Часы, Количество>>
        Map<String, Map<String, Long>> stat = this.createStat(entity.stream());
        // разбиение статистики по условиям
        this.splitStatByCondition(res, stat, themes);
        // разбиение статистики по условиям в процентах
        this.splitStatisticByPercent(res);
        // сбор статистики по времени срабатывания ip в миллисек
        this.apiTime(res, entity);
        return res;
    }

    /**
     * разбиение статистики по условиям
     *
     * @param res    {@link List<OnlineIVRDTO></>}
     * @param stat   статистика вся
     * @param themes список тематик
     */
    private void splitStatByCondition(List<OnlineIVRDTO> res, Map<String, Map<String, Long>> stat, List<Theme> themes) {
        // подготовка листа тематик Не обработанные сервисом ВК, но не дошедшие до оператора, шт.
        List<Theme> listThemeNon = this.createListNonSystem(themes);
        // посчитать общее количество за час
        res.add(this.toDto("Всего", this.countByCondition(stat, e -> true), true,
                (c, s, v2) ->
                        this.sum(v2))
        );
        // общее количество не обработанных
        res.add(this.toDto("Не обработанные сервисом ВК, но не дошедшие до оператора, шт.",
                countByCondition(stat, e -> listThemeNon.stream().map(s ->
                        e.equals(s.getName()))
                        .filter(s -> s).findFirst().orElse(false)), true,
                (c, s, v2) ->
                        this.sum(v2)));
        // не обработанные с разбивкой по тематикам
        this.createStatByTheme(stat, themes, res,
                e -> listThemeNon.stream().map(s ->
                        e.getValue().toLowerCase().equals(s.getValue().toLowerCase()))
                        .filter(s -> s).findFirst().orElse(false));
        // общее количество ушли к операторам
        res.add(this.toDto("Ушли в очередь к операторам, шт.",
                countByCondition(stat, e -> {
                    var r = true;
                    for (Theme theme : listThemeNon) {
                        if (theme.getName().toLowerCase().equals(e.toLowerCase())) {
                            r = false;
                            break;
                        }
                    }
                    return r;
                }), true,
                (c, s, v2) ->
                        this.sum(v2)));
        // ушли к  операторами с разбивкой по тематикам фильтром выступает listThemeNon
        this.createStatByTheme(stat, themes, res,
                e -> {
                    var r = false;
                    for (Theme theme : listThemeNon) {
                        if (theme.getValue().toLowerCase().equals(e.getValue().toLowerCase())) {
                            r = true;
                            break;
                        }
                    }
                    return !r;
                }
        );
    }

    private void splitStatisticByPercent(List<OnlineIVRDTO> res) {
        List<OnlineIVRDTO> percents = new ArrayList<>();
        OnlineIVRDTO temp = new OnlineIVRDTO();
        for (OnlineIVRDTO re : res) {
            if (re.getIsBlack()) {
                temp = re;
            }
            percents.add(this.splitStatisticByPercent(re.getIsBlack() ? res.get(0) : temp, re));
        }
        res.addAll(percents);
    }

    /**
     * добавление строк в процентном соотношении
     *
     * @param onlineRouBy   относительно какой считать проценты
     * @param onlineRowFrom строка, которая будет переведена в процентное соотношение
     */
    private OnlineIVRDTO splitStatisticByPercent(OnlineIVRDTO onlineRouBy, OnlineIVRDTO onlineRowFrom) {
        OnlineIVRDTO res = new OnlineIVRDTO();
        res.setIsBlack(onlineRowFrom.getIsBlack());
        res.setThemeName(onlineRowFrom.getThemeName());
        for (int i = 0; i < onlineRowFrom.getHourValue().length; i++) {
            res.getHourValue()[i] =
                    String.format("%.2f%s", Double.parseDouble(onlineRouBy.getHourValue()[i]) == 0 ? 0 :
                            Double.parseDouble(onlineRowFrom.getHourValue()[i]) * 100
                                    / Double.parseDouble(onlineRouBy.getHourValue()[i]), "%");
        }
        return res;
    }

    /**
     * подсчёт количества по условию (прошедшие не прошедшие до оператора)
     *
     * @param stat      вся статистика
     * @param condition условие
     * @return получить строку с результатом
     */
    private Map<String, Long> countByCondition(Map<String, Map<String, Long>> stat, Predicate<String> condition) {
        Map<String, Long> res = new HashMap<>();
        hours.forEach(e -> res.put(e, 0L));
        stat.keySet().stream().filter(condition).forEach(e -> {
            if (stat.containsKey(e) && stat.get(e) != null) {
                hours.forEach(k -> {
                    if (stat.get(e).containsKey(k) && stat.get(e).get(k) != null) {
                        res.put(k, res.get(k) + stat.get(e).get(k));
                    }
                });
            }
        });
        return res;
    }

    // добавление в лист строк по тематикам
    private void createStatByTheme(Map<String, Map<String, Long>> mapStat, List<Theme> themes,
                                   List<OnlineIVRDTO> onlineReports,
                                   Predicate<Theme> filter) {
        themes.stream().filter(filter).forEach(k -> {
                    onlineReports.add(this.toDto(k.getName(), mapStat.get(k.getName()),
                            false, (c, s, v2) ->
                                    this.sum(v2))
                    );
                }
        );
    }

    // сбор статистики по времени срабатывания ip в миллисек
    private void apiTime(List<OnlineIVRDTO> res, List<OnlineIVR> entity) {
        res.add(this.toDto(ServiceAnswer.LOYALITY_API_VIEW, createStatService(entity.stream(),
                OnlineIVR::getLOYALITY_API), true, this::avg));
        res.add(this.toDto(ServiceAnswer.STATUSES_API_HISTORY_VIEW, createStatService(entity.stream(),
                OnlineIVR::getSTATUSES_API_HISTORY), true, this::avg));
        res.add(this.toDto(ServiceAnswer.STATUSES_API_INFO_VIEW, createStatService(entity.stream(),
                OnlineIVR::getSTATUSES_API_INFO), true, this::avg));
        res.add(this.toDto(ServiceAnswer.RESERVE_ORDER_REST_REQUEST_VIEW, createStatService(entity.stream(),
                OnlineIVR::getRESERVE_ORDER_REST_REQUEST), true, this::avg));
        res.add(this.toDto(ServiceAnswer.CANCEL_ORDER_REST_REQUEST_VIEW, createStatService(entity.stream(),
                OnlineIVR::getCANCEL_ORDER_REST_REQUEST), true, this::avg));
        res.add(this.toDto(ServiceAnswer.STATUSES_CHANCEL_ORDER_REST_REQUEST_VIEW, createStatService(entity.stream(),
                OnlineIVR::getSTATUSES_CHANCEL_ORDER_REST_REQUEST), true, this::avg));
    }

    /**
     * создание объекта онлайн отчёт (1 объект 1 строка)
     *
     * @param name       наименование поля (первая ячейка)
     * @param hoursValue список значений по часам(остальные ячейки)
     * @param isBlack    булеан значение выделить стоку или нет
     * @param avg        функция для вычисления среднего значения или общей суммы
     * @return объекта онлайн отчёт (1 объект 1 строка) {@link OnlineIVRDTO}
     */
    public OnlineIVRDTO toDto(String name, Map<String, ? extends Number> hoursValue, Boolean isBlack, ThreeFunction<Integer, Double, OnlineIVRDTO> avg) {
        OnlineIVRDTO dto = new OnlineIVRDTO();
        dto.setIsBlack(isBlack);
        dto.setThemeName(name);
        Integer count = 0;
        Double sum = 0d;
        for (int i = 0; i < hours.size(); i++) {
            if (hoursValue != null && hoursValue.containsKey(hours.get(i)) && hoursValue.get(hours.get(i)) != null) {
                if (hoursValue.get(hours.get(i)) instanceof Double) {
                    count++;
                    sum += (Double) hoursValue.get(hours.get(i));
                    dto.getHourValue()[i] = String.format("%.2f", (Double) hoursValue.get(hours.get(i)));
                } else {
                    dto.getHourValue()[i] = hoursValue.get(hours.get(i)).toString();
                }
            } else {
                dto.getHourValue()[i] = "0";
            }
        }
        avg.apply(count, sum, dto);
        return dto;
    }

    /**
     * статистика по количеству за час с разбивкой на тематики
     */
    private Map<String, Map<String, Long>> createStat(Stream<OnlineIVR> onlineIVRStream) {
        return onlineIVRStream.collect(
                groupingBy(OnlineIVR::getTheme, Collectors.groupingBy(OnlineIVR::getHour, counting())));
    }

    /**
     * статистика по количеству за час с разбивкой на тематики
     */
    private Map<String, Double> createStatService(Stream<OnlineIVR> onlineIVRStream, Function<OnlineIVR, Double> getFieldValue) {
        return onlineIVRStream.collect(
                groupingBy(OnlineIVR::getHour, Collectors.averagingDouble(getFieldValue::apply)));
    }

    /**
     * вычисление суммы всех значений в массиве часов
     *
     * @param values {@link OnlineIVRDTO}
     */
    private void sum(OnlineIVRDTO values) {
        values.getHourValue()[24] = String.valueOf(Arrays.stream(values.getHourValue()).filter(Objects::nonNull).mapToLong(Long::parseLong)
                .reduce((sum, x) -> sum = sum + x).orElse(0L));
    }

    /**
     * вычисление среднего значения
     *
     * @param c количество элементов
     * @param s сумма элементов
     * @param v {@link OnlineIVRDTO}
     */
    private void avg(Integer c, Double s, OnlineIVRDTO v) {
        v.getHourValue()[24] = c > 0 ? String.format("%.2f", s / c) : String.format("%.2f", s);
    }

    /**
     * подготовка листа тематик Не обработанные сервисом ВК, но не дошедшие до оператора, шт.
     *
     * @param themes {@link List<Theme>}
     * @return {@link List<Theme>}
     */
    private List<Theme> createListNonSystem(List<Theme> themes) {
        return themes.stream().filter(e -> (e.getValue().toLowerCase().equals("hangupbeforefirst".toLowerCase())
                || e.getValue().toLowerCase().equals("goodend".toLowerCase()))).collect(Collectors.toList());
    }

    /**
     * посчитать общее количество за час
     */
    private Map<String, ? extends Number> createStatFullCount(Stream<OnlineIVR> onlineIVRStream) {
        return onlineIVRStream.collect(
                groupingBy(OnlineIVR::getHour, counting()));
    }
}
