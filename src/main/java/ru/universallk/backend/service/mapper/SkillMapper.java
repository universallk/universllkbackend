package ru.universallk.backend.service.mapper;


import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.Skill;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.service.dto.SkillDTO;
import ru.universallk.backend.service.dto.ThemeDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link App} and its DTO {@link ThemeDTO}.
 */
@Service
public class SkillMapper implements EntityMapper<SkillDTO, Skill> {

    public SkillDTO toDto(Skill skill) {
        if (skill == null) {
            return null;
        } else {
            SkillDTO skillDTO = new SkillDTO();
            skillDTO.setName(skill.getName());
            skillDTO.setId(skill.getId());
            skillDTO.setAppId(skill.getApp() != null ? skill.getApp().getId() : null);
            skillDTO.setValue(skill.getValue());
            return skillDTO;
        }
    }

    @Override
    public List<Skill> toEntity(List<SkillDTO> dtoList) {
        return dtoList == null ? new ArrayList<>() : dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<SkillDTO> toDto(List<Skill> entityList) {
        return entityList == null ? new ArrayList<>() : entityList.stream().map(this::toDto).collect(Collectors.toList());
    }


    public Skill toEntity(SkillDTO skillDTO) {
        if (skillDTO == null) {
            return null;
        } else {
            Skill skill = new Skill();
            skill.setName(skillDTO.getName());
            skill.setId(skillDTO.getId());
            skill.setApp(new App(skillDTO.getAppId()));
            skill.setValue(skillDTO.getValue());
            return skill;
        }
    }


    public Skill fromId(Long id) {
        if (id == null) {
            return null;
        }
        Skill skill = new Skill();
        skill.setId(id);
        return skill;
    }
}
