package ru.universallk.backend.service.mapper;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import ru.universallk.backend.domain.Skill;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.service.dto.audio.FullAudioIvr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import static ru.universallk.backend.service.dto.audio.ConstantSoundMessage.*;

/**
 * Class DataRowMapper
 *
 * @author Kseniya Dergunova
 * @since 17.04.2020
 */
public class FullAudioIvrRowMapper implements RowMapper<FullAudioIvr> {
    private final Logger log = LoggerFactory.getLogger(FullAudioIvrRowMapper.class);
    private final static String COLUMN_ID = "id";
    private final static String COLUMN_CONNECT_DATE = "connect_date";
    private final static String EXTRA_DATA = "extradata";
    private final static String COLUMN_PHONE = "phone";
    private final static String LAST_THEME = "last_theme";
    private final static String COLUMN_RECORD_FILE = "record_file";
    private final static String TIME_DIFF = "duration_time";
    private final static String SOUND_MESSAGE = "sounded_message";
    private final static String DISCONNECT_ACTION = "disconnect_action";
    private final String appName;
    private final List<Skill> skills;
    private final List<Theme> themes;

    public FullAudioIvrRowMapper(String appName, List<Skill> skills, List<Theme> themes) {
        this.appName = appName;
        this.skills = skills;
        this.themes = themes;
    }

    @Override
    public FullAudioIvr mapRow(ResultSet resultSet, int i) throws SQLException {
        FullAudioIvr dataRow = new FullAudioIvr();
        dataRow.setId(resultSet.getLong(COLUMN_ID));
        dataRow.setAppName(this.appName);
        Timestamp connectDate = resultSet.getTimestamp(COLUMN_CONNECT_DATE);
        if (connectDate != null) {
            dataRow.setConnectDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(connectDate));
        }
        Timestamp timeDiff = resultSet.getTimestamp(TIME_DIFF);
        if (timeDiff != null) {
            dataRow.setTimeDiff(new SimpleDateFormat("HH:mm:ss").format(timeDiff));
        }
        dataRow.setPhone(resultSet.getString(COLUMN_PHONE));
        dataRow.setRecordFile(resultSet.getString(COLUMN_RECORD_FILE));
        this.setThemes(dataRow, resultSet.getString(LAST_THEME));
        dataRow.setSoundMessage(resultSet.getString(SOUND_MESSAGE) == null ||
                resultSet.getString(SOUND_MESSAGE).equals(" ") ? NO_SOUND_MESSAGE : SOUND_MESSAGE);
        this.setDisconnectAction(dataRow, resultSet.getString(DISCONNECT_ACTION));
        try {
            this.timeAnswerByLoyalityApi(dataRow, resultSet.getString(EXTRA_DATA));
        } catch (Exception e) {
            log.error("не удалось проставить timeAnswerByLoyalityApi у dataRow = {}, jsonExtraDat = {}", dataRow, resultSet.getString(EXTRA_DATA));
            log.error(e.getMessage(), e);
        }
        log.debug("получаем объект dataRow = {}", dataRow);
        return dataRow;
    }

    private void setThemes(FullAudioIvr dataRow, String theme) {
        dataRow.setTheme(themes.stream().filter(e ->
                theme.toLowerCase().equals(e.getValue().toLowerCase())).findFirst().map(Theme::getName).orElse("Другое"));
    }

    private void timeAnswerByLoyalityApi(FullAudioIvr dataRow, String jsonExtraDat) throws JSONException {
        String rsl = "";
        if (jsonExtraDat != null && !jsonExtraDat.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonExtraDat);
            if (jsonObj.has("LOYALITY_API")) {
                dataRow.setSp_004_v3(jsonObj.getString("LOYALITY_API"));
            }
            if (jsonObj.has("VDN")) {
                String vdn = jsonObj.getString("VDN");
                dataRow.setSkill(
                        this.skills.stream().filter(e ->
                                vdn.equals(e.getValue())).findFirst().map(Skill::getName).orElse("")
                );
            }
        }
    }

    private void setDisconnectAction(FullAudioIvr dataRow, String disconnect_action) {
        if (disconnect_action != null
                && !disconnect_action.isEmpty()) {
            switch (disconnect_action.toLowerCase()) {
                case "transfer":
                    dataRow.setDisconectAction(TRANSFER);
                    break;
                case "hangup":
                    dataRow.setDisconectAction(END_BY_CALLER);
                    break;
                case "nearhangup":
                    dataRow.setDisconectAction(END_BY_VC);
                    break;
                default:
                    dataRow.setDisconectAction(UNDEFINED);
                    break;
            }
        }
    }
}
