package ru.universallk.backend.service.mapper;


import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.service.dto.KonturDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link App} and its DTO {@link KonturDTO}.
 */
@Service
public class ConturMapper implements EntityMapper<KonturDTO, Kontur> {

    public KonturDTO toDto(Kontur kontur) {
        if (kontur == null) {
            return null;
        } else {
            KonturDTO konturDTO = new KonturDTO();
            konturDTO.setName(kontur.getName());
            konturDTO.setId(kontur.getId());
            konturDTO.setApp(kontur.getApp() != null ? kontur.getApp().getId() : null);
            konturDTO.setKonturNumber(kontur.getKonturNumber());
            return konturDTO;
        }
    }

    @Override
    public List<Kontur> toEntity(List<KonturDTO> dtoList) {
        return dtoList == null ? new ArrayList<>() : dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<KonturDTO> toDto(List<Kontur> entityList) {
        return entityList == null ? new ArrayList<>() : entityList.stream().map(this::toDto).collect(Collectors.toList());
    }


    public Kontur toEntity(KonturDTO konturDTO) {
        if (konturDTO == null) {
            return null;
        } else {
            Kontur kontur = new Kontur();
            kontur.setName(konturDTO.getName());
            kontur.setId(konturDTO.getId());
            kontur.setApp(new App(konturDTO.getApp()));
            kontur.setKonturNumber(konturDTO.getKonturNumber());
            return kontur;
        }
    }


    public App fromId(Long id) {
        if (id == null) {
            return null;
        }
        App app = new App();
        app.setId(id);
        return app;
    }
}
