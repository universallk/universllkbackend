package ru.universallk.backend.service.reports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.service.CachingJdbcTemplate;
import ru.universallk.backend.service.dto.AppJDBC;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.reports.OnlineIVRDTO;
import ru.universallk.backend.service.mapper.OnlineIVRMapper;
import ru.universallk.backend.service.mapper.ReportMapper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * сервис дл сбора онлайн отчёта и отображения
 */
@Service
public class OnlineReportsService {
    private final CachingJdbcTemplate cachingJdbcTemplate;
    private final Logger log = LoggerFactory.getLogger(OnlineReportsService.class);
    private final OnlineIVRMapper onlineIVRMapper;

    public OnlineReportsService(CachingJdbcTemplate cachingJdbcTemplate, OnlineIVRMapper onlineIVRMapper) {
        this.cachingJdbcTemplate = cachingJdbcTemplate;
        this.onlineIVRMapper = onlineIVRMapper;
    }


    public List<OnlineIVRDTO> getOnlineByDate(FilterTotal filterTotal) {
        boolean[] isAnd = {false};
        StringBuilder queryParams = new StringBuilder("SELECT * ,\n" +
                "       mvideo_reports.get_last_theme(id)   as last_theme,     " +
                "  mvideo_reports.get_last_extradata(id)    as extradata\n" +
                "FROM app_statistic.call where "
        );
        if (this.cachingJdbcTemplate.getCaching().size() > 0) {
            if (filterTotal.getId() != null) {
                AppJDBC appJDBC = this.cachingJdbcTemplate.getCaching().get(filterTotal.getId());
                this.appendDate(queryParams, filterTotal, isAnd);
                this.setAnd(queryParams, isAnd);                     // поставим and
                queryParams.append(appJDBC.getApp().getKonturs().stream()    // добавляем список контуров
                        .map(Kontur::getKonturNumber).map(Object::toString).
                                collect(Collectors.joining(",", " app_id IN (", ")")));
                queryParams.append("  AND  char_length(phone) >= 10  AND disconnect_reason != 'error' ORDER BY connect_date ");
                return this.queryFiler(queryParams.toString(), appJDBC);
            } else {
                log.error("необходимо выбрать приложение");
                return null;
            }
        } else {
            log.error("все приложения выключены");
            return null;
        }
    }

    /**
     * выполнение запроса по подготовленным параметрам
     */
    private List<OnlineIVRDTO> queryFiler(String sql, AppJDBC appJDBC) {
        log.info("sql = {}", sql);
        List<OnlineIVRDTO> fullAudios = new ArrayList<>();
        try {
            fullAudios = new ArrayList<>(onlineIVRMapper.toDto(appJDBC.getJdbcTemplate().query(sql,
                    new ReportMapper(appJDBC.getApp().getThemes())), appJDBC.getApp().getThemes()));
        } catch (DataAccessException e) {
            log.error(e.getMessage(), e);
        }
        return fullAudios;
    }

    private void appendDate(StringBuilder sql, FilterTotal filterTotal, boolean[] isAnd) {
        if (filterTotal.getDateStart() != null) {
            sql.append("  connect_date >= '").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateStart()))).append("' ");
            isAnd[0] = true;
        }
        if (filterTotal.getDateEnd() != null) {
            this.setAnd(sql, isAnd);
            sql.append(" connect_date < '").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Timestamp.valueOf(filterTotal.getDateEnd()))).append("' ");
            isAnd[0] = true;
        }
    }

    private void setAnd(StringBuilder sql, boolean[] isAnd) {
        if (isAnd[0]) {
            sql.append(" AND ");
        }
    }

}
