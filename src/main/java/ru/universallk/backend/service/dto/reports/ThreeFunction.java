package ru.universallk.backend.service.dto.reports;

public interface ThreeFunction <T, U, B> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @return the function result
     */
   void  apply(T t, U u, B b);
}
