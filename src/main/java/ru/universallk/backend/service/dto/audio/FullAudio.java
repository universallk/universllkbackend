package ru.universallk.backend.service.dto.audio;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
@ToString
public class FullAudio {
    @Id
    private Long id;
    private String appName;
    private Long appId;
    private String phone;
    private String connectDate;
    private String recordFile;
}
