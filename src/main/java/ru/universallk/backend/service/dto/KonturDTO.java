package ru.universallk.backend.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class KonturDTO {
    private Long id;
    private String name;
    private Long konturNumber;
    @JsonIgnoreProperties("konturs")
    private Long app;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getKonturNumber() {
        return konturNumber;
    }

    public void setKonturNumber(Long konturNumber) {
        this.konturNumber = konturNumber;
    }

    public Long getApp() {
        return app;
    }

    public void setApp(Long app) {
        this.app = app;
    }
}
