package ru.universallk.backend.service.dto.reports;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class OnlineIVR {
    private String hour;
    private String theme;
    private final Integer count = 1;
    private Double LOYALITY_API = 0d;
    private Double STATUSES_API_HISTORY = 0d;
    private Double STATUSES_API_INFO = 0d;
    private Double RESERVE_ORDER_REST_REQUEST =  0d;
    private Double CANCEL_ORDER_REST_REQUEST = 0d;
    private Double STATUSES_CHANCEL_ORDER_REST_REQUEST = 0d;
}
