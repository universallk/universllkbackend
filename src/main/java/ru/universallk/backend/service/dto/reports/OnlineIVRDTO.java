package ru.universallk.backend.service.dto.reports;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author  Alexander Kaleganov
 * один объект это одна строка таблицы
 */
@Data
@EqualsAndHashCode
@ToString
public class OnlineIVRDTO {
    /**
     * первая ячейка в таблице наименование тематики
     */
    private String themeName; // первая ячейка
    /**
     * все остальные ячейки таблицы - значения за каждый час
     */
    private String[] hourValue = new String[25]; // остальные ячейки
    /**
     * boolean поле, если true то необходимо выделить  строку
     */
    private Boolean isBlack; // выделить или нет
}
