package ru.universallk.backend.service.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.AppType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A DTO for the {@link App} entity.
 */
@Data
@ToString
@EqualsAndHashCode
public class AppDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;
    @NotNull
    private List<KonturDTO> konturs = new ArrayList<>();

    private List<Long> remove = new ArrayList<>();
    @NotNull
    private String ip;
    @NotNull
    private String dataBase;
    @NotNull
    private String password;
    @NotNull
    private String login;
    @NotNull
    private boolean isEnabled;
    private String errorMessage;
    private AppType appType;
}
