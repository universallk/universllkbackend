package ru.universallk.backend.service.dto;



import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.AppType;
import ru.universallk.backend.service.filterUtil.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link App} entity. This class is used
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /apps?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AppCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;
    private AppTypeFilter appType;
    private StringFilter dataBase;

    private BooleanFilter isEnabled;

    public AppCriteria() {
    }

    public AppCriteria(AppCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.appType = other.appType == null ? null : other.appType.copy();
        this.dataBase = other.dataBase == null ? null : other.dataBase.copy();
        this.isEnabled = other.isEnabled == null ? null : other.isEnabled.copy();
    }

    @Override
    public AppCriteria copy() {
        return new AppCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public AppTypeFilter getAppType() {
        return appType;
    }

    public void setAppType(AppTypeFilter appType) {
        this.appType = appType;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public BooleanFilter getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(BooleanFilter isEnabled) {
        this.isEnabled = isEnabled;
    }

    public StringFilter getDataBase() {
        return dataBase;
    }

    public void setDataBase(StringFilter dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppCriteria that = (AppCriteria) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(appType, that.appType) &&
                Objects.equals(dataBase, that.dataBase) &&
                Objects.equals(isEnabled, that.isEnabled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, appType, dataBase, isEnabled);
    }


    /**
     * Class for filtering Priority
     */
    public static class AppTypeFilter extends Filter<AppType> {

        public AppTypeFilter() {
        }

        public AppTypeFilter(AppTypeFilter filter) {
            super(filter);
        }

        @Override
        public AppTypeFilter copy() {
            return new AppTypeFilter(this);
        }

    }

}
