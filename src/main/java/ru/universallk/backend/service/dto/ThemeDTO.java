package ru.universallk.backend.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Data
public class ThemeDTO {
    private Long id;
    private String name;
    private String value;
    private Long appId;
}
