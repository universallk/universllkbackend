package ru.universallk.backend.service.dto.reports;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.universallk.backend.service.PrepareUserService;
import ru.universallk.backend.service.ReportService;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MapperOnlineIvrDTOToExel {
    private static final Logger LOGGER = LoggerFactory.getLogger(MapperOnlineIvrDTOToExel.class);


    public List<OnlineIVRDTOFromExel> toDto(List<OnlineIVRDTO> list) {
        return list.stream().map(this::toDto).collect(Collectors.toList());
    }


    private OnlineIVRDTOFromExel toDto(OnlineIVRDTO report) {
        return new OnlineIVRDTOFromExel(report.getThemeName(), report.getIsBlack(),
                report.getHourValue()[0], report.getHourValue()[1],
                report.getHourValue()[2], report.getHourValue()[3],
                report.getHourValue()[4], report.getHourValue()[5],
                report.getHourValue()[6], report.getHourValue()[7],
                report.getHourValue()[8], report.getHourValue()[9],
                report.getHourValue()[10], report.getHourValue()[11],
                report.getHourValue()[12], report.getHourValue()[13],
                report.getHourValue()[14], report.getHourValue()[15],
                report.getHourValue()[16], report.getHourValue()[17],
                report.getHourValue()[18], report.getHourValue()[19],
                report.getHourValue()[20], report.getHourValue()[21],
                report.getHourValue()[22], report.getHourValue()[23], report.getHourValue()[24]
        );
    }
}
