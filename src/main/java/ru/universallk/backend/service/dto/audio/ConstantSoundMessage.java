package ru.universallk.backend.service.dto.audio;

public class ConstantSoundMessage {
    public final static String SOUND_MESSAGE = "sounded_message";
    public final static String NO_SOUND_MESSAGE = "no_sounded_message";
    public final static String END_BY_VC = "Звонок завершён Аленой";
    public final static String END_BY_CALLER = "Звонок завершён клиентом";
    public final static String TRANSFER = "Перевод на оператора";
    public final static String UNDEFINED = "Неопределено";
    public final static String TO_OTHER = "Ушли в очередь к операторам";
}
