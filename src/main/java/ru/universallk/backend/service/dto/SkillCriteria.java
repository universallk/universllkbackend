package ru.universallk.backend.service.dto;


import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.filterUtil.Criteria;
import ru.universallk.backend.service.filterUtil.Filter;
import ru.universallk.backend.service.filterUtil.LongFilter;
import ru.universallk.backend.service.filterUtil.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link App} entity. This class is used
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /apps?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SkillCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;
    private StringFilter value;
    private LongFilter appId;

    public LongFilter getAppId() {
        return appId;
    }

    public void setAppId(LongFilter appId) {
        this.appId = appId;
    }

    public SkillCriteria() {
    }

    public SkillCriteria(SkillCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.value = other.value == null ? null : other.value.copy();
        this.appId = other.appId == null ? null : other.appId.copy();
    }

    @Override
    public SkillCriteria copy() {
        return new SkillCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getValue() {
        return value;
    }

    public void setValue(StringFilter value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkillCriteria that = (SkillCriteria) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(appId, that.appId) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, value, appId);
    }
}
