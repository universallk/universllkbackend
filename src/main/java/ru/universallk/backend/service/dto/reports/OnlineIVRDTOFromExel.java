package ru.universallk.backend.service.dto.reports;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.InputStream;

/**
 * @author Alexander Kaleganov
 * один объект это одна строка таблицы
 */
@Data
@EqualsAndHashCode
@ToString
public class OnlineIVRDTOFromExel {
    private InputStream logo;

    /**
     * первая ячейка в таблице наименование тематики
     */
    private String themeName; // первая ячейка

    /**
     * boolean поле, если true то необходимо выделить  строку
     */
    private Boolean isBlack; // выделить или нет
    /**
     * все остальные ячейки таблицы - значения за каждый час
     */
    private String hourValue0 = ""; // остальные ячейки
    private String hourValue1 = ""; // остальные ячейки
    private String hourValue2 = ""; // остальные ячейки
    private String hourValue3 = ""; // остальные ячейки
    private String hourValue4 = ""; // остальные ячейки
    private String hourValue5 = ""; // остальные ячейки
    private String hourValue6 = ""; // остальные ячейки
    private String hourValue7 = ""; // остальные ячейки
    private String hourValue8 = ""; // остальные ячейки
    private String hourValue9 = ""; // остальные ячейки
    private String hourValue10 = ""; // остальные ячейки
    private String hourValue11 = ""; // остальные ячейки
    private String hourValue12 = ""; // остальные ячейки
    private String hourValue13 = ""; // остальные ячейки
    private String hourValue14 = ""; // остальные ячейки
    private String hourValue15 = ""; // остальные ячейки
    private String hourValue16 = ""; // остальные ячейки
    private String hourValue17 = ""; // остальные ячейки
    private String hourValue18 = ""; // остальные ячейки
    private String hourValue19 = ""; // остальные ячейки
    private String hourValue20 = ""; // остальные ячейки
    private String hourValue21 = ""; // остальные ячейки
    private String hourValue22 = ""; // остальные ячейки
    private String hourValue23 = ""; // остальные ячейки
    private String hourValueTotal = ""; // остальные ячейки

    public OnlineIVRDTOFromExel(String themeName, Boolean isBlack,
                                String hourValue0, String hourValue1,
                                String hourValue2, String hourValue3,
                                String hourValue4, String hourValue5,
                                String hourValue6, String hourValue7,
                                String hourValue8, String hourValue9,
                                String hourValue10, String hourValue11,
                                String hourValue12, String hourValue13,
                                String hourValue14, String hourValue15,
                                String hourValue16, String hourValue17,
                                String hourValue18, String hourValue19,
                                String hourValue20, String hourValue21,
                                String hourValue22, String hourValue23,
                                String hourValueTotal) {
        this.themeName = themeName;
        this.isBlack = isBlack;
        this.hourValue0 = hourValue0;
        this.hourValue1 = hourValue1;
        this.hourValue2 = hourValue2;
        this.hourValue3 = hourValue3;
        this.hourValue4 = hourValue4;
        this.hourValue5 = hourValue5;
        this.hourValue6 = hourValue6;
        this.hourValue7 = hourValue7;
        this.hourValue8 = hourValue8;
        this.hourValue9 = hourValue9;
        this.hourValue10 = hourValue10;
        this.hourValue11 = hourValue11;
        this.hourValue12 = hourValue12;
        this.hourValue13 = hourValue13;
        this.hourValue14 = hourValue14;
        this.hourValue15 = hourValue15;
        this.hourValue16 = hourValue16;
        this.hourValue17 = hourValue17;
        this.hourValue18 = hourValue18;
        this.hourValue19 = hourValue19;
        this.hourValue20 = hourValue20;
        this.hourValue21 = hourValue21;
        this.hourValue22 = hourValue22;
        this.hourValue23 = hourValue23;
        this.hourValueTotal = hourValueTotal;
    }
}
