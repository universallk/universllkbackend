package ru.universallk.backend.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.universallk.backend.domain.App;

/**
 * апп + jdbc
 */
@Data
@ToString
@EqualsAndHashCode
public class AppJDBC {
    private App app;
    private JdbcTemplate jdbcTemplate;

    public AppJDBC(App app, JdbcTemplate jdbcTemplate) {
        this.app = app;
        this.jdbcTemplate = jdbcTemplate;
    }

    public AppJDBC() {
    }
}
