package ru.universallk.backend.service.dto.audio;

import java.time.LocalDateTime;
import java.util.Objects;

public class FilterTotal {
    private Long id;
    private String phone;
    private LocalDateTime dateStart;
    private LocalDateTime dateEnd;
    private String theme;
    private String skill;
    private String soundMessage;
    private Long countAll;

    public FilterTotal setCountAll(Long countAll) {
        this.countAll = countAll;
        return this;
    }

    public Long getCountAll() {
        return countAll;
    }

    public Long getId() {
        return id;
    }

    public FilterTotal setId(Long id) {
        this.id = id;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public FilterTotal setPhone(String phone) {
        this.phone = phone;
        return this;

    }

    public LocalDateTime getDateStart() {
        return dateStart;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterTotal that = (FilterTotal) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(dateStart, that.dateStart) &&
                Objects.equals(dateEnd, that.dateEnd) &&
                Objects.equals(theme, that.theme) &&
                Objects.equals(skill, that.skill) &&
                Objects.equals(soundMessage, that.soundMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, dateStart, dateEnd, theme, skill, soundMessage);
    }

    @Override
    public String toString() {
        return "FilterTotal{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", count ='" + countAll + '\'' +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", theme='" + theme + '\'' +
                ", skill='" + skill + '\'' +
                ", soundMessage='" + soundMessage + '\'' +
                '}';
    }

    public FilterTotal setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
        return this;
    }

    public LocalDateTime getDateEnd() {
        return dateEnd;
    }

    public String getTheme() {
        return theme;
    }

    public FilterTotal setTheme(String theme) {
        this.theme = theme;
        return this;
    }

    public String getSkill() {
        return skill;
    }

    public FilterTotal setSkill(String skill) {
        this.skill = skill;
        return this;
    }

    public String getSoundMessage() {
        return soundMessage;

    }

    public FilterTotal setSoundMessage(String soundMessage) {
        this.soundMessage = soundMessage;
        return this;
    }

    public FilterTotal setDateEnd(LocalDateTime dateEnd) {
        this.dateEnd = dateEnd;
        return this;
    }
}
