package ru.universallk.backend.service.dto.audio;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Id;

@Data
@EqualsAndHashCode
@ToString
public class FullAudioIvr {
    @Id
    private Long id;
    private String appName;
    private String phone;
    private String connectDate;
    private String theme;
    private String sp_004_v3;
    private String skill;
    private String disconectAction;
    private String soundMessage;
    private String timeDiff;
    private String recordFile;

}
