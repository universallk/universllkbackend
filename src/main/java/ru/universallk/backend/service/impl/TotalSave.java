package ru.universallk.backend.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.service.AppService;
import ru.universallk.backend.service.dto.AppDTO;

import java.util.List;

@Service
public class TotalSave {
    private final KonturService konturService;
    private final AppService appService;

    public TotalSave(KonturService konturService, AppService appService) {
        this.konturService = konturService;
        this.appService = appService;
    }
@Transactional
    public AppDTO totalSave(AppDTO appDTO, List<Long> remove) {
        this.konturService.removeKonturListById(remove);
        return this.appService.save(appDTO);
//        return appDTO;
    }
}
