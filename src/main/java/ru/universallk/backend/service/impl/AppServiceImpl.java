package ru.universallk.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.repository.AppRepository;
import ru.universallk.backend.repository.KonturRepository;
import ru.universallk.backend.service.AppService;
import ru.universallk.backend.service.CachingJdbcTemplate;
import ru.universallk.backend.service.dto.AppDTO;
import ru.universallk.backend.service.mapper.AppMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link App}.
 */
@Service
@Transactional
public class AppServiceImpl implements AppService {

    private final Logger log = LoggerFactory.getLogger(AppServiceImpl.class);
    private final PrepareAppToSave prepareAppToSave;
    private final AppRepository appRepository;
    private final CachingJdbcTemplate cachingJdbcTemplate;
    private final AppMapper appMapper;
    private final KonturRepository konturRepository;

    public AppServiceImpl(PrepareAppToSave prepareAppToSave, AppRepository appRepository,
                          CachingJdbcTemplate cachingJdbcTemplate,
                          AppMapper appMapper,
                          KonturRepository konturRepository) {
        this.prepareAppToSave = prepareAppToSave;
        this.appRepository = appRepository;
        this.cachingJdbcTemplate = cachingJdbcTemplate;
        this.appMapper = appMapper;
        this.konturRepository = konturRepository;
    }

    /**
     * Save a app.
     *
     * @param appDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    @Transactional
    public AppDTO save(AppDTO appDTO) {
        log.debug("Request to save App : {}", appDTO);
        this.prepareAppToSave.findAllKonturByAppId(appDTO);
        if (appDTO.getErrorMessage() == null) {
            App app = appMapper.toEntity(appDTO);
            this.prepareAppToSave.mapping(app);
            app = appRepository.save(app);
            app.getSkills().forEach(c -> log.debug(c.toString()));
            app.getThemes().forEach(c -> log.debug(c.toString()));
            this.cachingJdbcTemplate.modify(app);
            return appMapper.toDto(app);
        } else {
            return appDTO;
        }

    }

    /**
     * Get all the apps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AppDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Apps");
        return appRepository.findAll(pageable)
                .map(appMapper::toDto);
    }

    /**
     * Get one app by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AppDTO> findOne(Long id) {
        log.debug("Request to get App : {}", id);
        return appRepository.findById(id)
                .map(appMapper::toDto);
    }

    /**
     * Delete the app by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete App : {}", id);
        this.konturRepository.deleteAllByAppId(id);
        appRepository.deleteById(id);
        this.cachingJdbcTemplate.removeFromCache(id);
    }

    @Transactional
    public List<App> findAllEnabled() {
        return this.appRepository.findAllByIsEnabledTrue().stream().peek(e -> {
                    e.getKonturs().forEach(c -> log.debug(c.toString()));
                    e.getSkills().forEach(c -> log.debug(c.toString()));
                    e.getThemes().forEach(c -> log.debug(c.toString()));
                }
               ).collect(Collectors.toList());
    }
}