package ru.universallk.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.repository.KonturRepository;
import ru.universallk.backend.service.dto.AppDTO;

import java.util.List;

@Service
public class PrepareAppToSave {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrepareAppToSave.class);

    private final KonturRepository konturRepository;

    public PrepareAppToSave(KonturRepository konturRepository) {
        this.konturRepository = konturRepository;
    }

    public void findAllKonturByAppId(AppDTO appDTO) {
        List<Kontur> list = this.konturRepository.findAllByAppId(appDTO.getId());
        LOGGER.error("list = " + list.toString());
        LOGGER.error("appDTO = " + appDTO.toString());
        list.forEach(e -> {
            for (int i = 0; i < appDTO.getKonturs().size(); i++) {
                if (appDTO.getKonturs().get(i).getId() == null && appDTO.getKonturs().get(i).getKonturNumber().equals(e.getKonturNumber())) {
                    appDTO.setErrorMessage("В приложении присутствует контур с id = " + appDTO.getKonturs().get(i).getKonturNumber());
                }
            }
        });
    }

    /**
     * сетаем приложение и контура
     *
     * @param app {@link App}
     */
    public void mapping(App app) {
        app.getKonturs().forEach(e -> e.setApp(app));
    }
}
