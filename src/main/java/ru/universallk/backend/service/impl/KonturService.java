package ru.universallk.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.repository.KonturRepository;

import java.util.List;
@Service
public class KonturService {
    private final KonturRepository konturRepository;
    private final Logger log = LoggerFactory.getLogger(KonturService.class);
    public KonturService(KonturRepository konturRepository) {
        this.konturRepository = konturRepository;
    }

    public void removeKonturListById( List<Long> konturs) {
        this.konturRepository.deleteAllByIdIn(konturs);
    }

}
