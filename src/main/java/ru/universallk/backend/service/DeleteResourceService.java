package ru.universallk.backend.service;

import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.DeleteResource;
import ru.universallk.backend.repository.DeleteResourceRepository;

import java.util.List;

@Service
public class DeleteResourceService {
    private final DeleteResourceRepository deleteResourceRepository;

    public DeleteResourceService(DeleteResourceRepository deleteResourceRepository) {
        this.deleteResourceRepository = deleteResourceRepository;
    }

    public DeleteResource save(DeleteResource deleteResource) {
        return this.deleteResourceRepository.save(deleteResource);
    }

    public void deleteResource(DeleteResource deleteResource) {
        this.deleteResourceRepository.delete(deleteResource);
    }

    public List<DeleteResource> findAl() {
        return this.deleteResourceRepository.findAll();
    }
}
