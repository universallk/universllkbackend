package ru.universallk.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.reports.MapperOnlineIvrDTOToExel;
import ru.universallk.backend.service.dto.reports.OnlineIVRDTOFromExel;
import ru.universallk.backend.service.reports.OnlineReportsService;
import ru.universallk.backend.service.util.ReportUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @version 0.0.1
 * @since 01 февр. 20
 */
@Service
public class ReportService {
    private final Logger log = LoggerFactory.getLogger(ReportService.class);
    private final MapperOnlineIvrDTOToExel mapperOnlineIvrDTOToExel;
    private final OnlineReportsService onlineReportsService;
    private final ReportUtil reportUtil;

    public ReportService(MapperOnlineIvrDTOToExel mapperOnlineIvrDTOToExel,
                         OnlineReportsService onlineReportsService, ReportUtil reportUtil) {
        this.mapperOnlineIvrDTOToExel = mapperOnlineIvrDTOToExel;
        this.onlineReportsService = onlineReportsService;
        this.reportUtil = reportUtil;
    }

    /**
     * осуществляется фильтрация для отчёта
     * <p>
     * Long fullAudioId,
     */
    @Transactional
    public Resource filterFromExport(FilterTotal filter, String pattern) {
        List<OnlineIVRDTOFromExel> res = this.mapperOnlineIvrDTOToExel.toDto(
                this.onlineReportsService.getOnlineByDate(filter));
        return this.reportUtil.exportOnlineReport(res,
                "onlineReport_" +
                        LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss")) + ".xlsx",
                pattern);

    }

}
