package ru.universallk.backend.service.util;

import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * класс для рандомной генерации логина и пароля
 */
@Service
public class RandomGenerator {

    public String generateFileName() {
        return new Random().ints(48, 122).filter(i ->
            (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(10)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
