package ru.universallk.backend.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.universallk.backend.service.CachingJdbcTemplate;
import ru.universallk.backend.service.DeleteFileService;
import ru.universallk.backend.service.impl.AppServiceImpl;

import javax.annotation.PostConstruct;

@Service
public class PostConstructClass {
    private final CachingJdbcTemplate cachingJdbcTemplate;
    private final Logger log = LoggerFactory.getLogger(PostConstructClass.class);
    private final AppServiceImpl appService;
private final DeleteFileService deleteFileService;
    public PostConstructClass(CachingJdbcTemplate cachingJdbcTemplate, AppServiceImpl appService, DeleteFileService deleteFileService) {
        this.cachingJdbcTemplate = cachingJdbcTemplate;

        this.appService = appService;
        this.deleteFileService = deleteFileService;
    }

    @PostConstruct
    public void startApp() {
        this.appService.findAllEnabled().forEach(this.cachingJdbcTemplate::modify);
        this.deleteFileService.deleteResource();
    }
}
