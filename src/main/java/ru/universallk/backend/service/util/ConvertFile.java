package ru.universallk.backend.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;

@Service
public class ConvertFile {
    private final Environment environment;
    private static final Logger log = LoggerFactory.getLogger(ConvertFile.class);
    private final RandomGenerator randomGenerator;
    public ConvertFile(Environment environment, RandomGenerator randomGenerator) {
        this.environment = environment;
        this.randomGenerator = randomGenerator;
    }

    //todo проверить в нагрузке попробывать сконвертировть в меньший размер
    public Resource convert(String property, String pathFile) {
        File file = new File(environment.getProperty(property) + "/" + pathFile);
        log.info("запрос на получение файла");
        log.info(file.getAbsolutePath());
        Resource res = null;
        if (file.exists()) {
            log.debug("существует gsm файл размер =  " + file.length());

            File newCallFile = new File("/tmp/" + this.randomGenerator.generateFileName() + ".wav");
            try {
                String cmd = "sox " + file.getAbsolutePath() +
                        " -e signed-integer -b 16 " + newCallFile.getAbsolutePath();
                Process process = Runtime.getRuntime().exec(cmd);
                process.waitFor();
                log.debug("получился размер = " +newCallFile.length());
                res = new UrlResource(Paths.get(newCallFile.getAbsolutePath()).toUri());
                log.info(res.toString());
            } catch (Exception e) {
                log.error("Удаленный хост принудительно разорвал существующее подключение при прослушивании монолога");
                if (newCallFile.delete()) {
                    log.debug("файл " + newCallFile.getName() + " удалён");
                }
                return res;
            }
        }
        return res;
    }
}
