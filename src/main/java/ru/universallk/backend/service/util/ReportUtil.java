package ru.universallk.backend.service.util;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import ru.universallk.backend.service.ReportService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import static net.sf.jasperreports.engine.xml.JRXmlLoader.load;

@Service
public class ReportUtil {
    private final Logger log = LoggerFactory.getLogger(ReportService.class);
    private final Environment environment;

    public ReportUtil(Environment environment) {
        this.environment = environment;
    }

    /**
     * экспорт отчёта в xlsx
     */
    public  <E> Resource exportOnlineReport(List<E> dataSource, String fileName, String pattern) {
        String path = environment.getProperty(pattern);
        Resource res = null;
        if (path != null) {
            InputStream inputStream = ReportService.class.getResourceAsStream(path);
            if (inputStream != null) {
                try {
                    File file = loadByteOutputStream(dataSource, inputStream, fileName);
                    if (file != null && file.exists()) {
                        res = new UrlResource(Paths.get(file.getAbsolutePath()).toUri());
                        log.info(res.toString());
                    } else {
                        log.error("file == null || !file.exists()");
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                log.error("inputStream == null");
            }
        } else {
            log.error("path == null");
        }
        return res;
    }

    /**
     * @param list        коллекция
     * @param inputStream jrxml
     * @param <E>         тип пока используется {@link E}
     * @return {@link ByteArrayOutputStream}
     */
    private <E> File loadByteOutputStream(List<E> list, InputStream inputStream, String fileName) {
        File result = new File("/tmp/" + fileName);
        try {
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            JasperReport jasperReport = JasperCompileManager.compileReport(load(inputStream));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<>(), dataSource);
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(result));
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            return result;
        } catch (JRException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

}
