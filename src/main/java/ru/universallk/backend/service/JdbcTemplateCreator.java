package ru.universallk.backend.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;
import ru.universallk.backend.domain.App;

/**
 * класс для создания jdbc  темлейтов
 */
@Service
public class JdbcTemplateCreator {

    public JdbcTemplate createJdbcTemplate(App app) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://" + app.getIp() + ":3306/" + app.getDataBase() + "?AllowPublicKeyRetrieval=True&useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow");
        dataSource.setUsername(app.getLogin());
        dataSource.setPassword(app.getPassword());
        return new JdbcTemplate(dataSource);
    }
}
