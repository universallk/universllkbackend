package ru.universallk.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversalLkBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniversalLkBackendApplication.class, args);
    }

}
