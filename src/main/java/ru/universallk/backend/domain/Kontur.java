package ru.universallk.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity(name = "kontur")
@Data
@EqualsAndHashCode(callSuper = true)
public class Kontur extends MappedSuperClass implements Serializable {
    @Column(name = "kontur_number")
    private Long konturNumber;
    @Column(name = "name")
    private String name;
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("konturs")
    private App app;

    @Override
    public String toString() {
        return "Kontur{" +
                "konturId=" + konturNumber +
                ", name='" + name + '\'' +
                '}';
    }
}
