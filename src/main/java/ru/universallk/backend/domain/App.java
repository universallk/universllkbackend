package ru.universallk.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A App.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "app")
public class App extends MappedSuperClass implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;
    @JsonIgnoreProperties("app")
    @OneToMany(mappedBy = "app", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Kontur> konturs = new ArrayList<Kontur>();
    @JsonIgnoreProperties("app")
    @OneToMany(mappedBy = "app", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Theme> themes = new ArrayList<Theme>();    @JsonIgnoreProperties("app")
    @OneToMany(mappedBy = "app", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Skill> skills = new ArrayList<Skill>();
    @NotNull
    @Column(name = "ip", nullable = false)
    private String ip;
    @NotNull
    @Column(name = "data_base", nullable = false)
    private String dataBase;
    @NotNull
    @Column(name = "login", nullable = false)
    private String login;
    @NotNull
    @Column(name = "password", nullable = false)
    private String password;
    @NotNull
    @Column(name = "is_enabled", nullable = false)
    private Boolean isEnabled;


    public App() {
    }
    @Enumerated(EnumType.STRING)
    @Column(name = "app_type")
    private AppType appType;

    public App(Long id) {
        this.setId(id);
    }

    @Override
    public String toString() {
        return "App{" +
                "name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", dataBase='" + dataBase + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", isEnabled=" + isEnabled +
                ", appType=" + appType +
                '}';
    }
}