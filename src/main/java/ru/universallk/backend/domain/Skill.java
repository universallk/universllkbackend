package ru.universallk.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Skill extends MappedSuperClass {
    @Column(name = "name")
    private String name;
    @Column(name ="value")
    private String value;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnore
    private App app;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    @Override
    public String toString() {
        return "Theme{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill theme = (Skill) o;
        return Objects.equals(name, theme.name) &&
                Objects.equals(value, theme.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }
}
