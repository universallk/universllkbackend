package ru.universallk.backend.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.relational.core.mapping.Column;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "delete_resource")
@Data
public class DeleteResource extends MappedSuperClass implements Serializable {
    @Column(value = "path")
    private String path;

    public DeleteResource() {
    }

    public DeleteResource(String path) {
        this.path = path;
    }

}
