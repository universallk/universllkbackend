package ru.universallk.backend.repository;


import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.universallk.backend.domain.Kontur;

import java.util.List;

/**
 * Spring Data  repository for the App entity.
 */
@Repository
public interface KonturRepository extends JpaRepository<Kontur, Long>, JpaSpecificationExecutor<Kontur> {
    List<Kontur> findAllByAppId(Long appId);

    List<Kontur> findAllByIdIn(List<Long> konturs);

    void deleteAllByAppId(Long appId);

    @Query("delete from kontur where id in (:konturs)")
    @Modifying
    void deleteAllByAppIdAndIdIn(@Param("konturs") List<Long> konturs);

    void deleteAllByIdIn(List<Long> list);
}
