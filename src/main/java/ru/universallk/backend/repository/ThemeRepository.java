package ru.universallk.backend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.universallk.backend.domain.Theme;

import java.util.List;

/**
 * Spring Data  repository for the App entity.
 */
@Repository
public interface ThemeRepository extends JpaRepository<Theme, Long>, JpaSpecificationExecutor<Theme> {
    List<Theme> findAllByAppId(Long appId);
}
