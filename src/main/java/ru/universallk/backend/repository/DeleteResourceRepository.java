package ru.universallk.backend.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.DeleteResource;

import java.util.List;

/**
 * Spring Data  repository for the App entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeleteResourceRepository extends JpaRepository<DeleteResource, Long>, JpaSpecificationExecutor<DeleteResource> {
}
