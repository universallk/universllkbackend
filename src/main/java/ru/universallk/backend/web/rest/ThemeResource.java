package ru.universallk.backend.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.AppQueryService;
import ru.universallk.backend.service.AppService;
import ru.universallk.backend.service.ThemeService;
import ru.universallk.backend.service.dto.*;
import ru.universallk.backend.service.impl.TotalSave;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link App}.
 */
@RestController
@RequestMapping("/api")
public class ThemeResource {

    private final Logger log = LoggerFactory.getLogger(ThemeResource.class);

    private static final String ENTITY_NAME = "theme";

    private final ThemeService themeService;

    public ThemeResource(ThemeService themeService) {
        this.themeService = themeService;
    }

    /**
     * {@code POST  /themes} : Create a new app.
     *
     * @param themeDTO the Theme to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new Theme, or with status {@code 400 (Bad Request)} if the app has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/themes")
    public ResponseEntity<ThemeDTO> createTheme(@Valid @RequestBody ThemeDTO themeDTO) throws URISyntaxException {
        log.debug("REST request to save App : {}", themeDTO);
        if (themeDTO.getId() != null) {
            log.error("A new app cannot already have an ID, {}", themeDTO.getId());
        }
        ThemeDTO result = this.themeService.save(themeDTO);
        return ResponseEntity.created(new URI("/api/themes/" + result.getId())).body(result);
    }


    @PutMapping(value = "/themes")
    public ResponseEntity<ThemeDTO> updateTheme(@Valid @RequestBody  ThemeDTO themeDTO) {
        log.info("REST request to update App : {}", themeDTO);
        if (themeDTO.getId() == null) {
            log.error("Invalid id {} {}", ENTITY_NAME, "idnull");
        }
        ThemeDTO result = this.themeService.save(themeDTO);
        return ResponseEntity.ok().body(result);
    }


    @GetMapping("/themes")
    public ResponseEntity<Page<ThemeDTO>> getAllThemes(ThemeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Apps by criteria: {}", criteria);
        Page<ThemeDTO> page = this.themeService.findByCriteria(criteria, pageable);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/themes/non-page")
    public ResponseEntity<List<ThemeDTO>> getAllThemes(ThemeCriteria criteria) {
        log.debug("REST request to get Apps by criteria: {}", criteria);
        List<ThemeDTO> page = this.themeService.findByCriteria(criteria);
        return ResponseEntity.ok().body(page);
    }

    /**
     * {@code GET  /apps/:id} : get the "id" app.
     *
     * @param id the id of the SkillDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the SkillDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/themes/{id}")
    public ResponseEntity<ThemeDTO> getTheme(@PathVariable Long id) {
        log.debug("REST request to get App : {}", id);
        Optional<ThemeDTO> appDTO = themeService.findOne(id);
        return ResponseEntity.ok(appDTO.orElse(new ThemeDTO()));
    }

    /**
     * {@code DELETE  /apps/:id} : delete the "id" app.
     *
     * @param id the id of the SkillDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/themes/{id}")
    public ResponseEntity<Void> deleteTheme(@PathVariable Long id) {
        log.debug("REST request to delete App : {}", id);
        this.themeService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
