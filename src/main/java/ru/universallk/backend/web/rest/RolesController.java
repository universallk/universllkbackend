package ru.universallk.backend.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.universallk.backend.aspect.AspectLogger;
import ru.universallk.backend.domain.Role;
import ru.universallk.backend.repository.RoleRepository;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RolesController {
    private final RoleRepository roleRepository;

    public RolesController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     *
     * @return получить список всех ролей
     */
    @GetMapping("/roles")
    public ResponseEntity<List<Role>> findAllRoles() {
        return ResponseEntity.ok().body(this.roleRepository.findAll());
    }
}
