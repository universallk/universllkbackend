package ru.universallk.backend.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.Skill;
import ru.universallk.backend.service.AppQueryService;
import ru.universallk.backend.service.AppService;
import ru.universallk.backend.service.SkillService;
import ru.universallk.backend.service.ThemeService;
import ru.universallk.backend.service.dto.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Skill}.
 */
@RestController
@RequestMapping("/api")
public class SkillResource {

    private final Logger log = LoggerFactory.getLogger(SkillResource.class);

    private static final String ENTITY_NAME = "skill";
    private final SkillService skillService;

    public SkillResource( SkillService skillService) {
        this.skillService = skillService;
    }

    /**
     * {@code POST  /themes} : Create a new Skill.
     *
     * @param skillDTO the SkillDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new SkillDTO, or with status {@code 400 (Bad Request)} if the app has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/skills")
    public ResponseEntity<SkillDTO> createSkill(@Valid @RequestBody SkillDTO skillDTO) throws URISyntaxException {
        log.debug("REST request to save Skill : {}", skillDTO);
        if (skillDTO.getId() != null) {
            log.error("A new Skill cannot already have an ID, {}", skillDTO.getId());
        }
        SkillDTO result = this.skillService.save(skillDTO);
        return ResponseEntity.created(new URI("/api/themes/" + result.getId())).body(result);
    }


    @PutMapping(value = "/skills")
    public ResponseEntity<SkillDTO> updateSkill(@Valid @RequestBody  SkillDTO skillDTO) {
        log.info("REST request to update Skill : {}", skillDTO);
        if (skillDTO.getId() == null) {
            log.error("Invalid id {} {}", ENTITY_NAME, "idnull");
        }
        SkillDTO result = this.skillService.save(skillDTO);
        return ResponseEntity.ok().body(result);
    }


    @GetMapping("/skills")
    public ResponseEntity<Page<SkillDTO>> getAllSkill(SkillCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Apps by criteria: {}", criteria);
        Page<SkillDTO> page = skillService.findByCriteria(criteria, pageable);
        return ResponseEntity.ok().body(page);
    }
    @GetMapping("/skills/non-page")
    public ResponseEntity<List<SkillDTO>> getAllSkillNonPage(SkillCriteria criteria) {
        log.debug("REST request to get Apps by criteria: {}", criteria);
        List<SkillDTO> page = skillService.findByCriteria(criteria);
        return ResponseEntity.ok().body(page);
    }


    /**
     * {@code GET  /apps/:id} : get the "id" app.
     *
     * @param id the id of the SkillDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the SkillDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/skills/{id}")
    public ResponseEntity<SkillDTO> getSkills(@PathVariable Long id) {
        log.debug("REST request to get skills : {}", id);
        Optional<SkillDTO> skillDTO = skillService.findOne(id);
        return ResponseEntity.ok(skillDTO.orElse(new SkillDTO()));
    }

    /**
     * {@code DELETE  /apps/:id} : delete the "id" app.
     *
     * @param id the id of the SkillDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/skills/{id}")
    public ResponseEntity<Void> deleteApp(@PathVariable Long id) {
        log.debug("REST request to delete App : {}", id);
        this.skillService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
