package ru.universallk.backend.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.DeleteFileService;
import ru.universallk.backend.service.ReportService;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.reports.OnlineIVRDTO;
import ru.universallk.backend.service.reports.OnlineReportsService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * REST controller for managing {@link App}.
 */
@RestController
@RequestMapping("/api")
public class ReportsResource {

    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);
    private final DeleteFileService deleteFileService;
    private final OnlineReportsService onlineReportsService;
    private final ReportService reportService;

    public ReportsResource(DeleteFileService deleteFileService, OnlineReportsService onlineReportsService, ReportService reportService) {
        this.deleteFileService = deleteFileService;
        this.onlineReportsService = onlineReportsService;
        this.reportService = reportService;
    }


    @GetMapping("/reports/ivr-preview")
    public ResponseEntity<List<OnlineIVRDTO>> getAllApps(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd) {
        FilterTotal filterTotal = new FilterTotal().setId(id).setDateStart(dateStart).setDateEnd(dateEnd);
        log.debug("REST request to get FilterTotal by criteria: {}", filterTotal);

        return ResponseEntity.ok().body(this.onlineReportsService.getOnlineByDate(filterTotal));
    }

    @GetMapping("/reports/download")
    public  ResponseEntity<Resource>  exportTranscriptions(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd) {
        FilterTotal filterTotal = new FilterTotal().setId(id).setDateStart(dateStart).setDateEnd(dateEnd);
        Resource file = this.reportService.filterFromExport(filterTotal, "reportsPattern.online");
        try {
            if (file != null && (file.exists() || file.isReadable())) {
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                        .body(file);
            } else {
                log.info("возвращаем null");
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "file.getFilename()" + "\"")
                        .body(file);
            }
        } finally {
            if (file != null && (file.exists() || file.isReadable())) {
                log.info("ресурс существует необходимо удалять");
                this.deleteFileService.pushToList(file);
            }
        }
    }

}
