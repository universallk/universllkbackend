package ru.universallk.backend.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.AppQueryService;
import ru.universallk.backend.service.AppService;
import ru.universallk.backend.service.dto.AppCriteria;
import ru.universallk.backend.service.dto.AppDTO;
import ru.universallk.backend.service.impl.KonturService;
import ru.universallk.backend.service.impl.TotalSave;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link App}.
 */
@RestController
@RequestMapping("/api")
public class AppResource {

    private final Logger log = LoggerFactory.getLogger(AppResource.class);

    private static final String ENTITY_NAME = "app";
    private final AppService appService;

    private final AppQueryService appQueryService;
    private final TotalSave totalSave;

    public AppResource( AppService appService, AppQueryService appQueryService, TotalSave totalSave) {
        this.appService = appService;
        this.appQueryService = appQueryService;
        this.totalSave = totalSave;
    }

    /**
     * {@code POST  /apps} : Create a new app.
     *
     * @param appDTO the appDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new appDTO, or with status {@code 400 (Bad Request)} if the app has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/apps")
    public ResponseEntity<AppDTO> createApp(@Valid @RequestBody AppDTO appDTO) throws URISyntaxException {
        log.debug("REST request to save App : {}", appDTO);
        if (appDTO.getId() != null) {
            log.error("A new app cannot already have an ID, {}", appDTO.getId());
        }
        AppDTO result = this.totalSave.totalSave(appDTO, new ArrayList<>());
        return ResponseEntity.created(new URI("/api/apps/" + result.getId())).body(result);
    }

    /**
     * {@code PUT  /apps} : Updates an existing app.
     *
     * @param appDTO the appDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated appDTO,
     * or with status {@code 400 (Bad Request)} if the appDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the appDTO couldn't be updated.
     */
    @PutMapping(value = "/apps")
    public ResponseEntity<AppDTO> updateApp(@Valid @RequestBody  AppDTO appDTO
                                          ) {
        log.info("REST request to update App : {}", appDTO);
        log.info("REST request to update App : {}", appDTO.getRemove());
        if (appDTO.getId() == null) {
            log.error("Invalid id {} {}", ENTITY_NAME, "idnull");
        }
        AppDTO result = this.totalSave.totalSave(appDTO, appDTO.getRemove());
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /apps} : get all the apps.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of apps in body.
     */
    @GetMapping("/apps")
    public ResponseEntity<Page<AppDTO>> getAllApps(AppCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Apps by criteria: {}", criteria);
        Page<AppDTO> page = appQueryService.findByCriteria(criteria, pageable);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/apps/enable")
    public ResponseEntity<List<AppDTO>> getAllApps(AppCriteria criteria) {
        log.debug("REST request to get Apps by criteria: {}", criteria);
        List<AppDTO> page = appQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(page);
    }


    /**
     * {@code GET  /apps/:id} : get the "id" app.
     *
     * @param id the id of the appDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the appDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/apps/{id}")
    public ResponseEntity<AppDTO> getApp(@PathVariable Long id) {
        log.debug("REST request to get App : {}", id);
        Optional<AppDTO> appDTO = appService.findOne(id);
        return ResponseEntity.ok(appDTO.orElse(new AppDTO()));
    }

    /**
     * {@code DELETE  /apps/:id} : delete the "id" app.
     *
     * @param id the id of the appDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/apps/{id}")
    public ResponseEntity<Void> deleteApp(@PathVariable Long id) {
        log.debug("REST request to delete App : {}", id);
        appService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
