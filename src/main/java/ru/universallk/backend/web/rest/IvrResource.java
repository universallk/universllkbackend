package ru.universallk.backend.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.service.DeleteFileService;
import ru.universallk.backend.service.FullAudioIVRService;
import ru.universallk.backend.service.FullAudioService;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.audio.FullAudio;
import ru.universallk.backend.service.dto.audio.FullAudioIvr;
import ru.universallk.backend.service.util.ConvertFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * REST controller for managing {@link App}.
 */
@RestController
@RequestMapping("/api")
public class IvrResource {

    private final Logger log = LoggerFactory.getLogger(IvrResource.class);
    private final DeleteFileService deleteFileService;

    private final FullAudioIVRService fullAudioIVRService;
    private final ConvertFile convertFile;

    public IvrResource(DeleteFileService deleteFileService, FullAudioIVRService fullAudioIVRService, ConvertFile convertFile) {
        this.deleteFileService = deleteFileService;
        this.fullAudioIVRService = fullAudioIVRService;
        this.convertFile = convertFile;
    }


    @GetMapping("/ivr")
    public ResponseEntity<Page<FullAudioIvr>> getAllApps(@RequestParam(required = false, value = "phone") String phone,
                                                      @RequestParam(value = "id") Long id,
                                                      @RequestParam(required = false, value = "skill") String skill,
                                                      @RequestParam(required = false, value = "countAll") Long countAll,
                                                      @RequestParam(required = false, value = "theme") String theme,
                                                      @RequestParam(required = false, value = "soundMessage") String soundMessage,
                                                      @RequestParam(required = false, value = "dateStart") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
                                                      @RequestParam(required = false, value = "dateEnd") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd, Pageable pageable) {
        FilterTotal filterTotal = new FilterTotal().setId(id).setPhone(phone).setDateStart(dateStart).setDateEnd(dateEnd)
                .setTheme(theme).setSkill(skill).setSoundMessage(soundMessage).setCountAll(countAll);
        log.debug("REST request to get FilterTotal by criteria: {}", filterTotal);
        Page<FullAudioIvr> page = this.fullAudioIVRService.findAll(filterTotal, pageable);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/ivr/audioPreview")
    public ResponseEntity<Resource> getAudioPreview(@RequestParam(value = "pathFile") String pathFile, HttpServletRequest request, HttpServletResponse response) {
        Resource res = this.convertFile.convert("app.fullrecordIVR.dir" ,pathFile);
        if (res == null) {
            res = this.convertFile.convert("app.fullrecordIVR2.dir", pathFile);
        }
        try {
            if (res != null && (res.exists() || res.isReadable())) {
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_TYPE, "audio/wav").body(res);
            } else {
                log.info("возвращаем null");
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION).body(null);
            }
        } finally {
            if (res != null && (res.exists() || res.isReadable())) {
                    log.info("ресурс существует необходимо удалять");
                    this.deleteFileService.pushToList(res);
                }
            }
        }
}
