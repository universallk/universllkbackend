package ru.universallk.backend.service.impl;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import ru.universallk.backend.domain.App;
import ru.universallk.backend.domain.Kontur;
import ru.universallk.backend.repository.KonturRepository;
import ru.universallk.backend.service.dto.AppDTO;
import ru.universallk.backend.service.dto.KonturDTO;
import ru.universallk.backend.service.mapper.AppMapper;
import ru.universallk.backend.service.mapper.ConturMapper;
import ru.universallk.backend.service.mapper.EntityMapper;

import javax.persistence.EntityManagerFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

@DisplayName("тестирование: AppServiceImplTest")
@TestPropertySource(locations = "classpath:application-h2.properties")
@SpringBootTest
class AppServiceImplTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppServiceImplTest.class);
    @Autowired
    AppServiceImpl appService;
    @Autowired
    AppMapper appMapper;
    @Autowired
    ConturMapper conturMapper;
    @Autowired
    KonturRepository konturRepository;
    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Test
    void save() {
        AppDTO appDTO = new AppDTO();
        appDTO.setLogin("ds");
        appDTO.setPassword("sadasd");
        Kontur kontur = new Kontur();
        kontur.setKonturNumber(12L);

        appDTO.setKonturs(Collections.emptyList());
        appDTO.setIp("192.168.1.10");
        appDTO.setDataBase("db");
        appDTO.setName("letual");
        appDTO.setEnabled(true);
        App app = this.appMapper.toEntity(appDTO);

        LOGGER.info(app::toString);
        AppDTO appDTO1 = appService.save(appDTO);
        LOGGER.info(appDTO1::toString);
    }
}