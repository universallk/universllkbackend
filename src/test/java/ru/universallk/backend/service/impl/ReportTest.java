package ru.universallk.backend.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.universallk.backend.domain.Theme;
import ru.universallk.backend.service.dto.reports.OnlineIVR;
import ru.universallk.backend.service.dto.reports.OnlineIVRDTO;
import ru.universallk.backend.service.mapper.OnlineIVRMapper;
import ru.universallk.backend.service.mapper.ReportMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.groupingBy;

public class ReportTest {
    private final String indent = "_______________________________________________________________|";
    private final List<String> hours = Arrays.asList("00", "1", "2", "3", "4", "5", "6", "7", "8",
            "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "Всего");
    private final ArrayList<Theme> themes =
            new ArrayList<>(Arrays.asList(new Theme("Обмен/возврат товаров", "mvideosap"),
                    new Theme("Mediamarkt", "mediamarkt"),
                    new Theme("iPhone", "iphone"),
                    new Theme("Юр.лица (VIP)", "vip"),
                    new Theme("Адреса", "address"),
                    new Theme("Кредит", "credit"),
                    new Theme("Услуги", "service"),
                    new Theme("Мобильное приложение", "mobileapp"),
                    new Theme("Акции и скидки", "promdis"),
                    new Theme("Бонусные рубли", "newsite"),
                    new Theme("Бонусные рубли. Проблемы.", "newsite_problem"),
                    new Theme("Бонусные рубли. Другие вопросы", "newsite_other"),
                    new Theme("Бонусные рубли. Регистрация БК", "newsite_register_card"),
                    new Theme("Подбор товара и услуг", "onlinestore"),
                    new Theme("Вопросы по оформленному заказу", "reserv"),
                    new Theme("Вопросы по оформленному заказу. Отмена заказа", "chancel_order"),
                    new Theme("Вопросы по оформленному заказу. Продление резерва", "reserve_order"),
                    new Theme("Вопросы по оформленному заказу. Восстановление Чека", "reserv_purchase_receipt_recovery"),
                    new Theme("Оператор", "operator"),
                    new Theme("NOMATCH", "NOMATCH"),
                    new Theme("Другое", "other"),
                    new Theme("Адреса магазинов", "address_shop_schedule"),
                    new Theme("Адреса сервисного центра", "address_authorized_service_center"),
                    new Theme("Претензии и жалобы", "ivanovo"),
                    new Theme("Адреса розничного магазина", "address_retail_store"),
                    new Theme("Trade-in", "promdis_util"),
                    new Theme("NOINPUT", "NOINPUT"),
                    new Theme("MAXSPEECHTIMEOUT", "MAXSPEECHTIMEOUT"),
                    new Theme("Заказы.Курьерская доставка", "reserv_courier_delivery"),
                    new Theme("Cashback", "newsite_cashback"),
                    new Theme("Вопросы по оформленному заказу. Товарное нарушение", "product_violation"),
                    new Theme("Вопросы по оформленному заказу. Быстрая доставка.", "fast_delivery"),
                    new Theme("Положили трубку до первого вопроса", "hangupbeforefirst"),
                    new Theme("Положили трубку после первого вопроса", "goodend")
            ));


    @Test
    public void createModel() {
        String sql = "SELECT * ,\n" +
                "       mvideo_reports.get_last_theme(id)   as last_theme,     " +
                "  mvideo_reports.get_last_extradata(id)    as extradata\n" +
                "FROM app_statistic.call where " +
                "  connect_date >= '2020-10-01 00:00:00'  AND  connect_date " +
                "< '2020-10-01 23:59:59' AND  " +
                "app_id IN (1)\n" +
                "AND  char_length(phone) >= 10  AND disconnect_reason != 'error' ORDER BY connect_date";
        List<OnlineIVR> rsl = this.createJdbcTemplate().query(sql, new ReportMapper(themes));
        rsl.forEach(e->{
            if (e.getHour().equals("00")) {
                System.out.println(e.getHour());
            }
        });
        long time = System.currentTimeMillis();
        this.printHeader();
        List<OnlineIVRDTO> res = new OnlineIVRMapper().toDto(rsl, this.themes);
        StringBuilder builder = new StringBuilder();
        res.forEach(e -> {
            builder.append(e.getThemeName()).append(this.indent.substring(e.getThemeName().length()));
            Arrays.stream(e.getHourValue()).forEach(k -> {
                this.row(builder, k);
            });
            System.out.println(builder.toString());
            builder.setLength(0);
        });
        System.out.println(System.currentTimeMillis()  - time + " Данные посчитаны");
    }


    /**
     * создание строки с данными
     *
     * @param builder строка с заволенной первой ячейкой
     * @param value   значение . которое необходимо добавить в ячейку
     */
    private void row(StringBuilder builder, String value) {
        String indent1 = "_________";
        builder.append("|").append(indent1.substring(value.length())).append(value).append("|");
    }

    /**
     * печать заголовка таблицы
     */
    private void printHeader() {
        StringBuilder stringBuilder = new StringBuilder(this.indent);
        this.hours.forEach(e -> {
            this.row(stringBuilder, e);
        });
        System.out.println(stringBuilder.toString());
    }

    private JdbcTemplate createJdbcTemplate() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://10.75.41.3:3306/app_statistic?AllowPublicKeyRetrieval=True&useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow");
        dataSource.setUsername("nesh");
        dataSource.setPassword("12345678");
        return new JdbcTemplate(dataSource);
    }
}
