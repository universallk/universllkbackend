package ru.universallk.backend.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.transaction.annotation.Transactional;
import ru.universallk.backend.service.dto.audio.FilterTotal;
import ru.universallk.backend.service.dto.reports.OnlineIVRDTOFromExel;
import ru.universallk.backend.web.rest.ReportsResource;

import javax.imageio.ImageIO;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static net.sf.jasperreports.engine.xml.JRXmlLoader.load;

class ReportServiceTest {
    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);

    @Test
    public void filterFromExport() {
        List<OnlineIVRDTOFromExel> res = new ArrayList<>();
        res.add(new OnlineIVRDTOFromExel("theme",
                true, "0","1",
                "2", "3","4",
                "5", "6","7",
                "8", "9","10",
                "11", "12","13",
                "14", "15","16",
                "17", "18","19",
                "20", "21","22", "23", "tot"
        ));
        this.exportOnlineReport(res,
                "onlineReport_" +
                        LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss")) + ".xlsx");

    }

    /**
     * экспорт отчёта в xlsx
     */
    private <E> Resource exportOnlineReport(List<E> dataSource, String fileName) {
        Resource res = null;
            InputStream inputStream = ReportService.class.getResourceAsStream("/report/mvideoOnlineReports1.jrxml");
            if (inputStream != null) {
                try {
                    File file = loadByteOutputStream(dataSource, inputStream, fileName);
                    if (file != null && file.exists()) {
                        res = new UrlResource(Paths.get(file.getAbsolutePath()).toUri());
                        log.info(res.toString());
                    } else {
                        log.error("file == null || !file.exists()");
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                log.error("inputStream == null");
            }
        return res;
    }

    /**
     * @param list        коллекция
     * @param inputStream jrxml
     * @param <E>         тип пока используется {@link E}
     * @return {@link ByteArrayOutputStream}
     */
    private <E> File loadByteOutputStream(List<E> list, InputStream inputStream, String fileName) {
        File result = new File("/tmp/" + fileName);
        try {
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            JasperReport jasperReport = JasperCompileManager.compileReport(load(inputStream));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<>(), dataSource);
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(result));
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//            configuration.setDetectCellType(true);//Set configuration as you like it!!
//            configuration.setCollapseRowSpan(false);
//            configuration.setShowGridLines(false);
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            return result;
        } catch (JRException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

}